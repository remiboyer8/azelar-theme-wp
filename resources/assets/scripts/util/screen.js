import UAParser from 'ua-parser-js';

// var firstTime = true;
export default {
  init() {
    this.addDeviceClasses();
    this.resizeRatio();
    // window.bodyScrollBar.addListener(this.resizeRatio);
    // bug resize radio je ne sais pas ou
    setTimeout(() => {
      this.resizeRatio();
    }, 500);

    // setTimeout(() => {
    //   this.resizeRatio();
    // }, 5000);

    // setTimeout(() => {
    //   this.resizeRatio();
    // }, 10000);


    window.addEventListener('resize', () => {
      this.resizeRatio();
    });
  },
  scrollListener(e) {
    console.log(e);
  },

  addDeviceClasses() {
    // ADD BODY CLASS UA PARSER
    const parser = new UAParser();
    const result = parser.getResult();
    try {
      if (result.os.name) {
        document.body.classList.add(
          result.os.name.split(' ').join().toLowerCase()
        );
      }
      if (result.browser.model) {
        document.body.classList.add(
          result.browser.model.split(' ').join().toLowerCase()
        );
      }
    } catch (error) {
      console.log('error adding body classes');
    }

    if (result.device.type !== undefined) {
      document.body.classList.add(result.device.type);
    }

    if (result.device.type === 'mobile' || result.device.type === 'tablet') {
      document.body.classList.add('touch');
    } else {
      document.body.classList.add('desktop');
    }

    $(window).resize(() => {
      if (
        !document.body.classList.contains('.Android') &&
        !document.body.classList.contains('.iOS')
      ) {
        this.vhFix();
      }
    });
  },
  vhFix() {
    // https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
    const vh =
      document.body.classList.contains('has-topbar') === true
        ? window.innerHeight -
          document.querySelector('#siteTopbar').getBoundingClientRect().height
        : window.innerHeight;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  },
  resizeRatio() {
    console.log('resize ratio')

    // if (firstTime) {
      // firstTime = false;
      let $el;

      $el = document.querySelectorAll('.card__metier .card-header');
      if ($el.length > 0) {
        $el.forEach((o) => {
          console.log(o.offsetWidth, $(o).width());
          o.style.height = `${o.offsetWidth}px`;
        });
      }

      $el = document.querySelectorAll('.ratio_1by1');
      if ($el.length > 0) {
        $el.forEach((o) => {
          o.style.height = `${o.offsetWidth}px`;
        });
      }

      $el = document.querySelectorAll('.ratio_16by9');
      if ($el.length > 0) {
        $el.forEach((o) => {
          o.style.height = `${(o.getBoundingClientRect().width * 9) / 16}px`;
          // console.log(o.getBoundingClientRect().width)
        });
      }

      $el = document.querySelectorAll('.ratio_25by10');
      $el.forEach((o) => {
        o.style.height = `${(o.getBoundingClientRect().width * 10) / 25}px`;
        // console.log(o.getBoundingClientRect().width)
      });

      $el = document.querySelectorAll('.ratio_25by7');
      $el.forEach((o) => {
        o.style.height = `${(o.getBoundingClientRect().width * 7.5) / 25}px`;
        // console.log(o.getBoundingClientRect().width)
      });

      $el = document.querySelectorAll('.ratio_9by16');
      if ($el.length > 0) {
        $el.forEach((o) => {
          o.style.height = `${(o.getBoundingClientRect().width * 16) / 9}px`;
        });
      }

      $el = document.querySelectorAll('.ratio_3by4');
      $el.forEach((o) => {
        console.log(
          o.getBoundingClientRect().width,
          (o.getBoundingClientRect().width * 4) / 3
        );
        o.style.height = `${(o.getBoundingClientRect().width * 4) / 3}px`;
      });

      $el = document.querySelectorAll('.ratio_4by3');
      if ($el.length > 0) {
        $el.forEach((o) => {
          o.style.height = `${(o.getBoundingClientRect().width * 3) / 4}px`;
        });
      }

      $el = document.querySelectorAll('.ratio_2by3');
      if ($el.length > 0) {
        $el.forEach((o) => {
          o.style.height = `${(o.getBoundingClientRect().width * 2) / 3}px`;
        });
      }
    // }
  },
};
