import Swiper from 'swiper/swiper-bundle';

var swiper;
var init = false;

export default {
  init() {
    new Swiper('.swiper-entrepreneur-cover', {
      loop: true,
      autoHeight: false,
      navigation: {
        nextEl: '.swiper-entrepreneur-cover .swiper-button-next',
        prevEl: '.swiper-entrepreneur-cover .swiper-button-prev',
      },
    });

    let sws;
    sws = document.querySelectorAll('.swiper-carrousel');
    [].forEach.call(sws, function (sw) {
      const s = document.querySelector(sw.getAttribute('data-controls'));
      new Swiper(sw, {
        slidesPerView: 'auto',
        spaceBetween: 30,
        watchOverflow: true,
        centerInsufficientSlides: true,
        // centeredSlides: true,
        // autoplay: {
        //   delay: 2000,
        //   disableOnInteraction: false,
        // },
        // freemode: true,
        navigation: {
          nextEl: s.querySelector('.swiper-button-next'),
          prevEl: s.querySelector('.swiper-button-prev'),
        },
        // breakpoints: {
        // 700: {
        //   spaceBetween: 30,
        // },
        // },
      });
    });

    new Swiper('.swiper-coverflow', {
      slidesPerView: 'auto',
      centeredSlides: true,
      spaceBetween: 0,
      mousewheel: false,
      initialSlide: 0,
      effect: 'coverflow',
      coverflowEffect: {
        rotate: 0,
        stretch: -40,
        slideShadows: false,
      },
      breakpoints: {
        700: {
          initialSlide: 1,
        },
      },
    });

    sws = document.querySelectorAll('.swiper-basic');
    [].forEach.call(sws, function (sw) {
      const s = document.querySelector(sw.getAttribute('data-controls'));
      new Swiper(sw, {
        slidesPerView: 1,
        spaceBetween: 0,
        preloadImages: false,
        lazy: {
          loadPrevNext: true,
          checkInView: true,
          scrollingElement: 'body',
        },
        loop: true,
        watchOverflow: true,
        navigation: {
          nextEl: s.querySelector('.swiper-button-next'),
          prevEl: s.querySelector('.swiper-button-prev'),
        },
      });
    });

    // new Swiper('#swiper-magic', {
    //   direction: 'vertical',
    // });
    // this.addListenners();
  },

  swiperMode() {
    let mobile = window.matchMedia('(min-width: 0px) and (max-width: 768px)');
    let tablet = window.matchMedia(
      '(min-width: 769px) and (max-width: 1024px)'
    );
    let desktop = window.matchMedia('(min-width: 1025px)');

    console.log(init, swiper);
    // Enable (for mobile)
    if (mobile.matches) {
      if (!init) {
        init = true;
        swiper = new Swiper('.swiper-coverflow', {
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 0,
          mousewheel: false,
          grabCursor: true,
          initialSlide: 0,
          effect: 'coverflow',
          coverflowEffect: {
            rotate: 0,
            stretch: -40,
            slideShadows: false,
          },
          breakpoints: {
            700: {
              // spaceBetween: 60,
            },
          },
        });
      }
    } else if (tablet.matches) {
      swiper.destroy();
      init = false;
    } else if (desktop.matches) {
      swiper.destroy();
      init = false;
    }
  },

  addListenners() {
    window.addEventListener('load', () => {
      this.swiperMode();
    });

    window.addEventListener('resize', () => {
      this.swiperMode();
    });
  },
};
