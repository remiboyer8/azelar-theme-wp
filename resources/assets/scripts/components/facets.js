import 'select2';
import 'select2/dist/js/i18n/fr';

export default {
  init() {
    $.fn.select2.defaults.set('theme', 'classic');
    $.fn.select2.defaults.set('language', 'fr');
    // $.fn.select2.defaults.set('debug', true);

    var skills = [];
    this.filterCards(skills);

    if ($('.js-select-skills').length > 0) {
      $('.js-select-skills').select2({
        // closeOnSelect: false,
        dropdownParent: $('#dropdown-skills'),
        width: '100%',
        matcher: this.matchCustom,
      });

      $('.js-select-skills').on('select2:select', (e) => {
        // setTimeout(() => {
        skills.push(e.params.data);
        this.filterCards(skills);
        // }, 50);
      });

      $('.js-select-skills').on('select2:unselect', (e) => {
        // setTimeout(() => {
        skills.pop(e.params.data);
        this.filterCards(skills);
        // }, 50);
      });
    }

    if ($('.js-select-person').length > 0) {
      $('.js-select-person').select2({
        dropdownParent: $('#dropdown-person'),
        width: '100%',
        minimumInputLength: 1,
        // matcher: this.matchCustom,
      });

      //  reset field
      $('.js-select-person').val(null).trigger('change');

      $('.js-select-person').on('select2:select', (e) => {
        // console.log(e.params.data);
        location.href = e.params.data.id;
      });

      $('.js-select-person input').on('change', function () {
        $('.js-select-person').select2('open');
      });

      $('.js-select-person').on('select2:open', function () {
        setTimeout(() => {
          document
            .querySelector('#dropdown-person .select2-search__field')
            .focus();
        }, 50);
      });
    }

    // $('.js-select-skills').on('select2:selecting', function () {
    //   console.log(
    //     $('.select-skills-container').find('.select2-search__field').val()
    //   );

    //   setTimeout(() => {
    //     $('.js-select-skills').find('.select2-search__field').val('');
    //   }, 50);
    // });

    $('.section__search').addClass('show');
  },

  matchCustom(params, data) {

    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
      return '';
    }



    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
      return null;
    }

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    // if (data.text.indexOf(params.term) > -1) {
    if (data.text.indexOf(params.term) > -1 || data.element.getAttribute('data-alt').indexOf(params.term) > -1) {
      // var modifiedData = $.extend({}, data, true);
      // modifiedData.text += ' (matched)';

      // You can return modified objects from here
      // This includes matching the `children` how you want in nested data sets
      return data;
    }

    // Return `null` if the term should not be displayed
    return null;
  },

  filterCards(a) {
    const $els = document.querySelectorAll('.card__person-container');

    if (a.length > 0) {
      //hide all
      [].forEach.call($els, ($el) => {
        // $el.style.left = $el.getBoundingClientRect().left;
        // $el.style.top = $el.getBoundingClientRect().top;
        // $el.classList.add('hide');
        $($el).fadeOut();
      });

      // show only selected
      for (let index = 0; index < a.length; index++) {
        const metier = a[index];
        [].forEach.call($els, ($el) => {
          // if ($el.dataset.metier === metier.id) {
          if ($el.classList.contains(metier.id)) {
            // $el.style.left = 'auto';
            // $el.style.top = 'auto';
            // $el.classList.remove('hide');
            $($el).stop().fadeIn();
          }
        });
      }
    } else {
      //show all
      [].forEach.call($els, function ($el) {
        // $el.style.left = 'auto';
        // $el.style.top = 'auto';
        // $el.classList.remove('hide');
        $($el).stop().fadeIn();
      });
    }
  },
};
