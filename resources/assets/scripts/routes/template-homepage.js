export default {
  init() {
    window.scrollTo(0, 0);
    this.initCover();
  },

  initCover() {
    let resizeTimer;
    window.addEventListener('resize', () => {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(() => {
        this.resizeCoverDesktop();
      }, 100);
    });
    this.resizeCoverDesktop();
    this.appear();

    // animate
    setTimeout(() => {
      this.resizeCoverDesktop();
    }, 500);

    // setInterval(() => {
    //   this.live();
    // }, Math.random(100) * 3000);
  },
  // live() {
  //   const svgCover = $('.cover-desktop');
  //   const draws = svgCover.find('.cover_mask');
  //   const len = draws.length;
  //   const rand = Math.floor(Math.random(len) * 100);
  //   const el = draws[rand];
  //   $(el).removeClass('open');

  //   setTimeout(() => {
  //     $(el).addClass('open');
  //   }, 500);
  // },

  appear() {
    const svgCover = $('.cover-desktop');
    svgCover.find('.cover_mask').each((index, el) => {
      setTimeout(() => {
        $(el).addClass('open');
      }, Math.random(100) * 1000);
    });
  },
  resizeCoverDesktop() {
    const $coverHome = $('#coverHomeDesktop');
    const containerRatio = $coverHome.width() / $coverHome.height();
    const svgRatio = 1.436594202898;
    $coverHome.removeClass('portrait landscape');
    const orientation = containerRatio <= svgRatio ? 'portrait' : 'landscape';
    console.log('resize cover :: ', orientation);
    $coverHome.addClass(orientation);
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
