// import Vimeo from '@vimeo/player';

var player = null;
var aSoundclouds = [];
export default {
  init() {
    // this.initVimeo();
    this.initSoundcloud();
    this.addListeners();
  },
  addListeners() {
    // modal video
    $('#modal-video').on('show.bs.modal', function (event) {
      const button = $(event.relatedTarget);
      // const iframe =  $('#modal-video').querySelector('#vimeo');
      // console.log(button, button.data('vimeo'));
      if (player == null) {
        player = new window.Vimeo.Player('vimeo', {
          id: button.data('vimeo'),
        });
      } else {
        player.destroy().then(() => {
          new window.Vimeo.Player('vimeo', {
            id: button.data('vimeo'),
          });
        })
      }

      player.play();
      for (let i = 0; i < aSoundclouds.length; ++i) {
        aSoundclouds[i].pause();
      }
    });
    $('#modal-video').on('hide.bs.modal', function (event) {
      console.log('modal video hide', event);
      player.pause();
    });

    //soundcloud play;
    for (let i = 0; i < aSoundclouds.length; ++i) {
      let el = aSoundclouds[i];
      console.log(el);
      el.bind('SC.Widget.Events.PLAY_PROGRESS', (e) => {
        console.log(e);
      });
      el.bind('SC.Widget.Events.PLAY', (e) => {
        console.log(e);
        aSoundclouds.forEach((other) => {
          if (other !== aSoundclouds[i]) {
            other.pause();
          }
        });
      });
    }
  },
  // initVimeo() {
  //   var iframe = document.querySelector('#vimeo');
  //   player = new window.Vimeo.Player(iframe);

  //   player.on('play', function () {
  //     console.log('Played the video');
  //   });

  //   player.getVideoTitle().then(function (title) {
  //     console.log('title:', title);
  //   });
  // },
  initSoundcloud() {
    var soundClouds = document.querySelectorAll('.soundcloud');
    for (let i = 0; i < soundClouds.length; ++i) {
      aSoundclouds.push(window.SC.Widget(soundClouds[i]));
    }
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
