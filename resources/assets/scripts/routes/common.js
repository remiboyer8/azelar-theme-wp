import Swipers from '../components/swipers';
import Dom from '../controllers/dom';
import Animations from '../controllers/animation';
import Forms from '../components/forms';
import Facets from '../components/facets';
import Screen from '../util/screen';


export default {
  init() {
    Screen.init();
    Swipers.init();
    Dom.init();
    Animations.init();
    Forms.init()
    Facets.init()
  },

  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
