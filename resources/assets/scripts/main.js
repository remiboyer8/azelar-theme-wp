console.log('ready');
// import external dependencies
import 'jquery';
import objectFitImages from 'object-fit-images';
// import Scrollbar from 'smooth-scrollbar';
import { gsap} from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';
gsap.registerPlugin(ScrollTrigger);
gsap.registerPlugin(ScrollToPlugin);

// Import everything from autoload
import './autoload/**/*';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/template-homepage';
import singleEntrepreneur from './routes/singleEntrepreneur';
import aboutUs from './routes/about';

objectFitImages();

//smoothscroll
// window.bodyScrollBar = Scrollbar.init(document.querySelector('.scroller'), {
//   damping: 0.1,
//   delegateTo: document,
//   emitEvents: true,
// });

// Scrollbar.initAll();

//smoothTrigger
// ScrollTrigger.scrollerProxy('.scroller', {
//   scrollTop(value) {
//     if (arguments.length) {
//       window.bodyScrollBar.scrollTop = value;
//     }
//     return window.bodyScrollBar.scrollTop;
//   },
// });
// window.bodyScrollBar.addListener(ScrollTrigger.update);

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  singleEntrepreneur,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});


// Load Events
jQuery(document).ready(() => {
  console.log('ready');
  routes.loadEvents();
});
