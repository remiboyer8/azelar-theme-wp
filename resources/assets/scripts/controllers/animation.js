import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';

// import ScrollbarPlugin from 'smooth-scrollbar';

var oldScrollPosition = 0;
var stepScrollPosition = 0;

export default {
  init() {
    // if ($(window).width() > 992) {
    // this.initScrollSlide();
    this.initScrollAnimation();
    this.initScrollAnimationDesktop();
    // }

    this.scrollObserver();
    this.addListeners();
  },

  addListeners() {
    window.addEventListener('scroll', () => {
      this.scrollObserver();
    });
  },

  hide(elem) {
    gsap.set(elem, { autoAlpha: 0 });
  },
  scrollObserver() {
    //animate navigation
    if (window.scrollTop > 200) {
      if (window.scrollTop > stepScrollPosition) {
        document.body.classList.add('js-hide-header');
      } else {
        document.body.classList.remove('js-hide-header');
      }
    } else {
      document.body.classList.remove('js-hide-header');
    }

    if (window.scrollTop > oldScrollPosition) {
      oldScrollPosition = window.scrollTop;
    } else {
      stepScrollPosition = window.scrollTop;
    }
  },
  initScrollSlide() {
    var sections = gsap.utils.toArray('.scroll-slide-panel');

    sections.forEach((section, i) => {
      var tl = gsap.timeline({
        scrollTrigger: {
          trigger: 'section.section__scroll-slide',
          scroller: 'body',
          start: () => 'top -' + 500 * (i + 0.5),
          end: () => '+=' + 500,
          scrub: true,
          toggleActions: 'play pause reverse pause',
          invalidateOnRefresh: true,
          // snap: 0.1,
          snap: {
            snapTo: 'labels',
            delay: 0,
            directional: false,
            inertia: true,
          },
        },
      });
      if (i == sections.length - 1) {
        tl.to(section, { duration: 0.3, x: '0%', opacity: 1 }, '>');
        tl.addLabel('last');
      } else {
        tl.to(section, { duration: 0.3, x: '0%', opacity: 1 }, '>')
          .addLabel('show')
          .to(section, { duration: 0.3, x: '-50%', opacity: 0 }, '>');
      }
    });

    ScrollTrigger.create({
      trigger: 'section.section__scroll-slide',
      scroller: 'body',
      scrub: true,
      // markers: true,
      pin: true,
      anticipatePin: 1,
      start: () => 'top top',
      end: () => '+=' + (sections.length + 1) * 500,
      invalidateOnRefresh: true,
    });
  },

  initScrollAnimation() {
    gsap.utils.toArray('.gs_counter').forEach((elem) => {
      // this.hide(elem);
      ScrollTrigger.create({
        trigger: elem,
        scroller: 'body',
        scrub: true,
        start: () => 'center bottom',
        onEnter: (e) => {
          this.startCounter(elem, e);
        },
      });
    });
  },

  initScrollAnimationDesktop() {
    console.log('initScrollAnimationDesktop');
    // let scrollAnimDelay = 0;
    // let scrollAnimDelayTimeout = null;
    gsap.utils.toArray('.gs_reveal').forEach((elem) => {
      this.hide(elem);
      ScrollTrigger.create({
        trigger: elem,
        scroller: 'body',
        onEnterBack: () => {
          // console.log('onEnterBack :: ');
        },
        onRefresh: () => {},
        onEnter: () => {
          // console.log('onEnter :: ', scrollAnimDelay, scrollAnimDelayTimeout);
          this.animateFrom(elem);
          // scrollAnimDelay++;
        },
      });
    });
  },
  animateFrom(elem, direction) {
    direction = direction || 1;
    var x = 0,
      y = direction * 40;
    if (elem.classList.contains('gs_reveal_fromLeft')) {
      x = -40;
      y = 0;
    } else if (elem.classList.contains('gs_reveal_fromRight')) {
      x = 40;
      y = 0;
    } else if (elem.classList.contains('gs_reveal_transformLeft')) {
      x = '-100%';
      y = 0;
    } else if (elem.classList.contains('gs_reveal_transformRigt')) {
      x = '100%';
      y = 0;
    }
    elem.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
    elem.style.opacity = '0';

    gsap.fromTo(
      elem,
      { x: x, y: y, autoAlpha: 0 },
      {
        duration: 0.6,
        x: 0,
        y: 0,
        autoAlpha: 1,
        ease: 'ease',
        overwrite: false,
      }
    );
  },

  startCounter(elem, e) {
    if (e.direction === 1) {
      console.log('startCounter', $(elem).attr('data-count'));
      const countTo = $(elem).attr('data-count');
      $(elem).text(0);
      $({
        countNum: parseInt($(elem).text()),
      }).animate(
        {
          countNum: countTo,
        },
        {
          duration: 2000,
          easing: 'swing',
          step: function () {
            // console.log(Math.floor(this.countNum));
            $(elem).text(Math.floor(this.countNum));
          },
          complete: function () {
            console.log(this.countNum);
            $(elem).text(this.countNum);
          },
        }
      );
    }
  },
};
