// all JS- classes
export default {
  init() {
    $('.js-toggle-class').click(function () {
      const cssClass = $(this).data('class');

      if ($(this).hasClass(cssClass)) {
        $(this).removeClass(cssClass);
      } else {
        $(this).addClass(cssClass);
      }
    });

    $('.js-toggle-element-class').click(function () {
      const $el = $($(this).data('element'));
      const cssClass = $(this).data('class');
      const removeClass = $(this).data('remove-class');
      $el.removeClass(removeClass);

      if ($el.hasClass(cssClass)) {
        $el.removeClass(cssClass);
      } else {
        $el.addClass(cssClass);
      }
    });

    $('.js-remove-body-scroll').click(() => {
      const y = document.documentElement.scrollTop || document.body.scrollTop;

      if (y > 0) {
        this.top = y;
      }

      if (!$('body').hasClass('noscroll')) {
        $('body').css('top', `${-this.top}px`).addClass('noscroll');
      } else {
        $('body').css('top', '0px').removeClass('noscroll');
        window.scroll(0, this.top);
      }
    });

    $('.js-scroll-to').click(function () {
      const $el = $($(this).data('scroll'));
      const callback = $(this).data('callback');
      // console.log('scroll to', $el, $el.offset().top);
      $('html, body').animate(
        {
          scrollTop: $el.offset().top - 100,
        },
        1000,
        'swing',
        () => {
          if (callback === 'openCollapse') {
            $el.collapse('show');
          }
        }
      );
    });
  },
};
