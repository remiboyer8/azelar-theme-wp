@extends('layouts.app')

@section('aside')
@include('partials.aside-entrepreneur')
@endsection
@section('content')

<header class="header__entrepeneur">
  <div class="container gs_reveal ">
    <div class="content bg-white ">
      <div class="wrapper">
        <h1 class="page-title">{{$data->prenom}} {{$data->nom}}</h1>
        @if(!empty($data->nom_commercial) || !empty($data->baseline))
        <h2 class="page-subtitle">
          @if(!empty($data->nom_commercial))
          <span class="d-block d-lg-none py-1">{{$data->nom_commercial}}</span>
          @endif
          <span class="d-none d-lg-block">{{$data->baseline}}</span>
        </h2>
        @endif
      </div>
      <div class="tags">
        @if(!empty($data->hashtag_1))
        <div class="tag tag__red">#{{$data->hashtag_1}}</div>
        <div class="sep sep__blue"></div>
        @endif
        @if(!empty($data->hashtag_2))
        <div class="tag tag__yellow">#{{$data->hashtag_2}}</div>
        <div class="sep sep__redlight"></div>
        @endif
        @if(!empty($data->hashtag_3))
        <div class="tag tag__blue">#{{$data->hashtag_3}}</div>
        @endif
      </div>
    </div>

    @if(!empty($data->email))
    {{-- <div class="row"></div> --}}
    <a href="mailto:{{$data->email}}" class="btn gs_reveal">
      {{-- @include('svg.mail') --}}
      @include('svg.mail')
      <span>{{$global_options['entrepreneur_entrer_en_contact']}}</span>
    </a>
    @endif

    {{-- mobile only --}}
    {{-- mobile only --}}
    <div class="coords d-lg-none text-center">
      <img src="{{wp_get_attachment_image_url( $data->vignette , 'm'  )}}"
        srcset="{{wp_get_attachment_image_srcset( $data->vignette )}}" alt="{{$data->prenom}} {{$data->nom}}"
        sizes="{{wp_get_attachment_image_sizes( $data->vignette )}}" loading="lazy" class="portrait">
      <div>
        @if(!empty($data->site_internet))
        <a href="{{$data->site_internet}}" target="_blank">{{preg_replace( "#^[^:/.]*[:/]+#i", "",
          $data->site_internet)}}</a>
        @endif
        @if(!empty($data->telephone))
        @if(!empty($data->site_internet))•@endif
        <a href="tel:{{str_replace(' ', '', $data->telephone)}}">{{$data->telephone}}</a>
        @endif
      </div>
      @if(!empty($data->email))
      <a href="mailto:{{$data->email}}">{{$data->email}}</a>
      @endif
      <div class="motif">
        @foreach ($poles_metiers as $k=>$pole_metier)
        @foreach ($data->poles_metiers as $pole_metier_id)
        @if($pole_metier_id == $pole_metier['id'] && isset($pole_metier['image']) && $pole_metier['image'] > 0)
        <a href="{{$pole_metier['url']}}" class="d-inline">
          <img src="{{wp_get_attachment_image_url( $pole_metier['image'] , 's'  )}}" loading="lazy"
            alt="{{get_post_meta($pole_metier['image'], '_wp_attachment_image_alt', TRUE)}}">
        </a>
        @endif
        @endforeach
        @endforeach
        {{-- <img src="@asset('images/datas/motif1.png')" alt="">
        <img src="@asset('images/datas/motif2.png')" alt="">
        <img src="@asset('images/datas/motif3.png')" alt=""> --}}
      </div>
      @if(!empty($data->lieu_activite))
      <div class="location">{{$data->lieu_activite}}</div>
      @endif
      <div class="rs">
        <div class="socials">
          @if(is_array($data->liens))
          @foreach($data->liens as $type=>$url)
          @if(!empty($url))
          <a href="{{$url}}" target="_blank">@include('svg.'.$type)</a>
          @endif
          @endforeach
          @endif
          {{-- <a href="#">@include('svg.facebook')</a>
          <a href="#">@include('svg.linkedin')</a>
          <a href="#">@include('svg.instagram')</a> --}}
        </div>
      </div>
    </div>
  </div>
</header>
@include('partials.content-entrepreneur')

<section class="footer__entrepreneur">
  <div class="container">
    <a href="{{get_permalink($global_options['annuaire_page'])}}" class="btn btn-annuaire btn-primary">
      @include('svg.annuaire')
      <span>{{$global_options['entrepreneur_back_annuaire_texte']}}</span>
    </a>
  </div>
</section>


{{--
<pre>
  @php
  var_dump($data);
  @endphp
  </pre> --}}

@endsection
