{{--
  Template Name: Contact Template
--}}

@extends('layouts.app')

@section('content')

@include('partials.sections.section-contact', [
  'is_h1' => true,
])

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>0 ])

@endsection
