{{--
  Template Name: Coopérative
--}}

@extends('layouts.app')

@section('content')

@include('partials.sections.section-bloc',
[
  'is_h1' => true,
  'section_data' => $page_fields['naissance'],
])



@include('partials.sections.section-twoblocs', ['section_data' => $page_fields['revendications']])

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])


@include('partials.sections.section-bloc',
[
  'section_data' => $page_fields['autonomie'],
])


@include('partials.sections.button', ['section_data' => $page_fields['bouton_chroniques']])
{{--
 ['label'=>'les chroniques d\'azelar', 'link'=>'#',
'icon'=>'','class'=>'btn-secondary' ]) --}}

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@include('partials.sections.section-text', ['section_data' => $page_fields['savoirs']])

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.button', ['section_data' => $page_fields['bouton_rejoindre']])
 {{-- ['label'=>'Nous rejoindre', 'link'=>'#',
'icon'=>'','class'=>'btn-secondary' ]) --}}

@if(isset($page_fields['temoignages']['display']) && $page_fields['temoignages']['display'])
@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])
@endif

@include('partials.sections.section-testimony', ['section_data' => $page_fields['temoignages']])

{{-- @include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>0 ]) --}}

@if(isset($page_fields['temoignages']['display']) && $page_fields['temoignages']['display'])
@include('partials.sections.button', ['section_data' => $page_fields['bouton_contacter']])
@endif
{{-- , ['label'=>'Nous contacter', 'link'=>'#',
'icon'=>'','class'=>'btn-secondary' ]) --}}

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])


@endsection
