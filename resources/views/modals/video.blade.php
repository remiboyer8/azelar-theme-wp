<div class="modal fade" id="modal-video" tabindex="-1">
  <div class="close" data-dismiss="modal" aria-label="fermer">
    <svg xmlns="http://www.w3.org/2000/svg" width="28.414" height="28.414" viewBox="0 0 28.414 28.414">
      <g transform="translate(-348.793 -18.793)">
        <path d="M26.293,27.707l-27-27L.707-.707l27,27Z" transform="translate(349.5 19.5)" fill="#fff" />
        <path d="M.707,27.707-.707,26.293l27-27L27.707.707Z" transform="translate(349.5 19.5)" fill="#fff" />
      </g>
    </svg>
  </div>
  <div class="modal-dialog modal-xl modal-dialog-centered">

    <div class="modal-content">
      <div class="embed-responsive embed-responsive-16by9">
        <div id="vimeo"></div>
        <script src="https://player.vimeo.com/api/player.js"></script>
      </div>
    </div>
  </div>
</div>

{{-- https://player.vimeo.com/video/{video_id} --}}
