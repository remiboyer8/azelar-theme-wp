@extends('layouts.app')

@section('content')


@if(isset($page_fields['titre']) && !empty($page_fields['titre']))
  @include('partials.page-header', ['title' => $page_fields['titre']])
@endif

@if(isset($page_fields['contenu']) && !empty($page_fields['contenu']))
<section class="section section__text section__text-left gs_reveal">
  <div class="container">
    {!!$page_fields['contenu']!!}
  </div>
</div>
@endif

@endsection
