<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')

<body @php body_class() @endphp>
  @php do_action('get_header') @endphp
  @include('partials.header')
  <div class="wrap" role="document">
    <aside class="sidebar">
      @include('partials.navigation')
      @yield('aside')
    </aside>
    <main class="main scroller">
      {{-- <div class="main-content"> --}}
      @yield('content')
      {{-- </div> --}}
      @php do_action('get_footer') @endphp
      @include('partials.footer')
      @php wp_footer() @endphp
    </main>

    @include('modals.video')
  </div>

</body>

</html>
