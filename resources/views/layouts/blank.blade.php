<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <main class="main">
      @yield('content')
    </main>
    @php wp_footer() @endphp
  </body>

</html>
