@extends('layouts.app')

@section('content')


<article class="article">
  <div class="container">
    @if(isset($page_fields['image']) && $page_fields['image'] > 0)
    <div class="article-img-top">
      <img src="{{wp_get_attachment_image_url( $page_fields['image'] , 'l'  )}}" class="img-fluid mx-auto"
        data-srcset="{{wp_get_attachment_image_srcset( $page_fields['image'] )}}"
        sizes="{{wp_get_attachment_image_sizes($page_fields['image'])}}"
        alt="{{get_post_meta($page_fields['image'], '_wp_attachment_image_alt', TRUE)}}">
    </div>
  </div>
  @endif
  {{-- <img src="@asset('images/datas/img.jpg')" class="img-fluid mx-auto" alt=""> --}}
  <section class="section gs_reveal section__text-left">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-8">
          @if(isset($page_fields['titre_long']) && !empty($page_fields['titre_long']))
          <h1 class="section-title">{!!$page_fields['titre_long']!!}</h1>
          @endif
          @if(isset($page_fields['intro']) && !empty($page_fields['intro']))
          <div class="section-text">{!!$page_fields['intro']!!}</div>
          @endif
        </div>
      </div>
    </div>
  </section>

  <div class="container">
    @include('partials.sections.dots', ['class'=>'dots__left'])
  </div>

  <div class="gabarits-container">
    <div class="sections">
      @if(isset($page_fields['sections']['sections']) && is_array($page_fields['sections']['sections']) &&
      sizeof($page_fields['sections']['sections']) > 0)
      @include('partials.content-sections', ['sections' => $page_fields['sections']['sections']])
      @endif
    </div>
  </div>



</article>

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@include('partials.sections.button', ['section_data' => [
'lien' => [
'title' => $global_options['blog_btn_back'],
'url' => get_permalink( get_option( 'page_for_posts' ) )
]
]])


@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@endsection
