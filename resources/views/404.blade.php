@extends('layouts.app')

@section('content')

  @if(isset($global_options['404_titre']) && !empty($global_options['404_titre']))
    @include('partials.page-header', ['title' => $global_options['404_titre']])
  @endif

  @if(isset($global_options['404_texte']) && !empty($global_options['404_texte']))
    <section class="section section__text gs_reveal">
      <div class="container">
        {!!$global_options['404_texte']!!}
      </div>
    </div>
  @endif
@endsection
