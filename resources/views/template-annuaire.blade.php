{{--
  Template Name: Annuaire
--}}

@extends('layouts.app')

@section('aside')
@include('partials.aside-facets')
@endsection
@section('content')
  @if(isset($page_fields['titre']) && !empty($page_fields['titre']))
    @include('partials.page-header', ['title' => $page_fields['titre']])
  @else
    @php
    $get_queried_object = get_queried_object();
    $term_fields = false;
    if(isset($get_queried_object->term_id)) {
      $term_fields = get_fields( 'poles_metiers_'.$get_queried_object->term_id);
    }
    // var_dump($term_fields);
    @endphp
    @if((isset($term_fields['annuaire_titre']) && !empty($term_fields['annuaire_titre'])) || (isset($term_fields['annuaire_soustitre']) && !empty($term_fields['annuaire_soustitre'])))
      @include('partials.page-header', [
        'title' => (isset($term_fields['annuaire_titre'])) ? $term_fields['annuaire_titre'] : '',
        'subtitle' => (isset($term_fields['annuaire_soustitre'])) ? $term_fields['annuaire_soustitre'] : ''
        ])
    @endif
  @endif
  @include('partials.list-entrepreneur')

  @php
  $get_queried_object = get_queried_object();
  @endphp

  @if(isset($get_queried_object->term_id ) && $get_queried_object->term_id > 0)
  <div class="my-5">
    @include('partials.sections.button', ['section_data' => [
      'lien' => [
        'title' => $global_options['annuaire_btn_all'],
        'url' => get_permalink( $global_options['annuaire_page'] )
        ]
    ]])
  </div>
  @endif


@endsection
