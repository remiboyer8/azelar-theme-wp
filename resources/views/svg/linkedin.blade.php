<svg xmlns="http://www.w3.org/2000/svg" width="15.74" height="14.78" viewBox="0 0 15.74 14.78">
  <path
    d="M3.47,14.78H.4V5H3.47Zm-1.61-11h0A1.7,1.7,0,0,1,0,2.17V2A1.86,1.86,0,0,1,1.7,0,1.87,1.87,0,1,1,2,3.73Zm13.88,11H12.26V9.7c0-1.33-.54-2.24-1.74-2.24a1.76,1.76,0,0,0-1.66,1.2,2.16,2.16,0,0,0-.07.8v5.31H5.34s0-9,0-9.81H8.79V6.5a3.13,3.13,0,0,1,3.06-1.63c2.18,0,3.89,1.41,3.89,4.45Z"
    fill="#3c446a" />
</svg>
