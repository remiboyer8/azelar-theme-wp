<svg xmlns="http://www.w3.org/2000/svg" width="43.2" height="30.4" viewBox="0 0 43.2 30.4">
  <path fill="#fff" d="M18.3,5.1L0,0v25.3l17.9,5.1l0,0h25.3V5.1H18.3z M17.9,28.7L1.7,23.9v-22l16.2,4.8V28.7z M41.5,28.7h-22v-22h22
	V28.7z" />
  <rect x="32.1" y="10.3" fill="#fff" width="6.5" height="1.7" />
  <rect x="32.1" y="14.7" fill="#fff" width="6.5" height="1.7" />
  <rect x="32.1" y="19" fill="#fff" width="6.5" height="1.7" />
  <rect x="32.1" y="23.4" fill="#fff" width="6.5" height="1.7" />
  <rect x="22.4" y="10.4" fill="#fff" width="6.5" height="1.7" />
  <rect x="22.4" y="14.8" fill="#fff" width="6.5" height="1.7" />
  <rect x="22.4" y="19.2" fill="#fff" width="6.5" height="1.7" />
  <rect x="22.4" y="23.5" fill="#fff" width="6.5" height="1.7" />
</svg>
