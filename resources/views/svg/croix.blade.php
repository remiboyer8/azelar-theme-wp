<svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28.41 28.41">

  <polygon id="7f1f03ef-0c39-4976-8f24-6eac54c2b288" data-name="croix"
    points="28.41 1.41 27 0 14.21 12.79 1.41 0 0 1.41 12.79 14.21 0 27 1.41 28.41 14.21 15.62 27 28.41 28.41 27 15.62 14.21 28.41 1.41"
    fill="#fff" />

</svg>
