<svg width="138" height="138" viewBox="0 0 138 138" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="arts-appliques" clip-path="url(#arts-appliquesclip0)">
<path id="Vector" d="M137.139 137.139V0L0.000152588 0V137.139H137.139Z" fill="#F5BC1C"/>
<path id="filler" d="M0 137.14L70.049 68.499L137.139 -0.000976562H0V137.14Z" fill="#47436A"/>
<path id="Vector_2" d="M24.3179 112.545H112.668V24.593L24.3179 112.545Z" fill="#141939"/>
<path id="Vector_3" d="M112.666 24.594H24.3179V112.546L112.666 24.594Z" fill="#DF2333"/>
<g id="Group">
<g id="Group_2">
<path id="Vector_4" d="M123.306 21.15H13.833V28.036H123.306V21.15Z" fill="white"/>
<path id="Vector_5" d="M123.306 109.102H13.833V115.988H123.306V109.102Z" fill="white"/>
</g>
<g id="Group_3">
<path id="Vector_6" d="M27.9158 16.721H21.0298V120.418H27.9158V16.721Z" fill="white"/>
<path id="Vector_7" d="M116.108 16.721H109.222V120.418H116.108V16.721Z" fill="white"/>
</g>
</g>
<path id="circle" d="M52.4121 68.57C52.4121 71.7657 53.3598 74.8897 55.1353 77.5469C56.9107 80.2041 59.4342 82.2751 62.3867 83.498C65.3392 84.721 68.5881 85.041 71.7224 84.4175C74.8568 83.7941 77.7358 82.2552 79.9955 79.9954C82.2553 77.7357 83.7942 74.8566 84.4177 71.7223C85.0411 68.5879 84.7211 65.3391 83.4982 62.3866C82.2752 59.4341 80.2042 56.9106 77.5471 55.1351C74.8899 53.3596 71.7659 52.412 68.5701 52.412C64.2861 52.4165 60.1789 54.1203 57.1497 57.1495C54.1204 60.1788 52.4166 64.286 52.4121 68.57V68.57Z" fill="#FACDC0"/>
</g>
<defs>
<clipPath id="arts-appliquesclip0">
<rect width="137.139" height="137.14" fill="white"/>
</clipPath>
</defs>
</svg>
