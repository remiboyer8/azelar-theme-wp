<svg xmlns="http://www.w3.org/2000/svg" width="19.02" height="19.89" viewBox="0 0 19.02 19.89">

  <path
    d="M1.3,0,0,3.46V17.29H4.75v2.6h2.6l2.59-2.6h3.89L19,12.1V0ZM3,1.73H17.29v9.51l-3,3H9.51l-2.59,2.6V14.27H3Zm4.75,8.65H9.51V5.19H7.78Zm4.75,0h1.73V5.19H12.53Z"
    fill="#3c446a" />

</svg>
