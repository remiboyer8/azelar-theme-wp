<svg width="44" height="31" viewBox="0 0 44 31" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0)">
    <path
      d="M18.3 5.1L0 0V25.3L17.9 30.4H43.2V5.1H18.3ZM17.9 28.7L1.7 23.9V1.9L17.9 6.7V28.7ZM41.5 28.7H19.5V6.7H41.5V28.7Z"
      fill="white" />
    <path d="M38.6 10.3H32.1V12H38.6V10.3Z" fill="white" />
    <path d="M38.6 14.7H32.1V16.4H38.6V14.7Z" fill="white" />
    <path d="M38.6 19H32.1V20.7H38.6V19Z" fill="white" />
    <path d="M38.6 23.4H32.1V25.1H38.6V23.4Z" fill="white" />
    <path d="M28.9 10.4H22.4V12.1H28.9V10.4Z" fill="white" />
    <path d="M28.9 14.8H22.4V16.5H28.9V14.8Z" fill="white" />
    <path d="M28.9 19.2H22.4V20.9H28.9V19.2Z" fill="white" />
    <path d="M28.9 23.5H22.4V25.2H28.9V23.5Z" fill="white" />
  </g>
  <defs>
    <clipPath id="clip0">
      <rect width="43.2" height="30.4" fill="white" />
    </clipPath>
  </defs>
</svg>
