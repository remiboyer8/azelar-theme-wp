<svg xmlns="http://www.w3.org/2000/svg" width="1586" height="1104" viewBox="0 0 1586 1104" class="{{$class}}">
  <path d="M896.5,275.5H620.24V.5H896.5Z" fill="#3a3759" />
  <path
    d="M466.31,53.58A72.26,72.26,0,0,0,464,153.46L566.29,51.3A72.43,72.43,0,0,0,466.31,53.58ZM465,154.52c-.34-.35-.67-.71-1-1.06l-.06,0,2.35,2.35.06-.06-1.07-1c-.1-.09-.19-.19-.29-.29ZM566.35,51.24l-.06.06c.35.3.69.62,1,.94l0,0c.44.44.85.9,1.28,1.35l.06-.05Z"
    fill="#141939" />
  <path
    d="M621.28,0V206.79H518L620.18,104.72,569,53.58l-2.35-2.34L517.78,2.44,466.59,53.58l-51.2,51.14,48.85,48.8,2.35,2.34,51,50.93H414.18V0Z"
    fill="#141939" />
  <path
    d="M466.31,53.58A72.26,72.26,0,0,0,464,153.46L566.29,51.3A72.43,72.43,0,0,0,466.31,53.58ZM465,154.52c-.34-.35-.67-.71-1-1.06l-.06,0,2.35,2.35.06-.06-1.07-1c-.1-.09-.19-.19-.29-.29ZM566.35,51.24l-.06.06c.35.3.69.62,1,.94l0,0c.44.44.85.9,1.28,1.35l.06-.05ZM517.29,206.79l.21.21.21-.21Z"
    fill="#fef3f0" />
  <path d="M466.31,53.58l-51.2,51.14L464,153.51l.05,0A72.25,72.25,0,0,1,466.31,53.58Z" fill="#fef3f1" />
  <path d="M566.29,51.3l.05-.06L517.5,2.44,466.31,53.58A72.44,72.44,0,0,1,566.29,51.3Z" fill="#fef3f1" />
  <path d="M568.69,53.58l-.05.05A72.36,72.36,0,0,1,466.37,155.8l-.06.06,51,50.92h.43L619.89,104.72Z" fill="#fef3f1" />
  <path d="M466.37,155.8l-.06.06c-.35-.35-.69-.7-1-1C465.66,155.15,466,155.48,466.37,155.8Z" fill="#f5bc1c" />
  <path d="M566.29,51.3,464,153.46A72.35,72.35,0,0,1,566.29,51.3Z" fill="#f5bc1c" />
  <path d="M568.69,53.58l-.05.05c-.43-.46-.85-.91-1.29-1.36Z" fill="#f5bc1c" />
  <path
    d="M566.34,153.51a72.45,72.45,0,0,1-100,2.29c-.36-.32-.7-.65-1.06-1-.1-.09-.19-.2-.29-.3-.34-.34-.68-.7-1-1.06L566.29,51.3c.35.3.69.62,1,.94l0,0,1.28,1.36A72.24,72.24,0,0,1,566.34,153.51Z"
    fill="#df2333" />
  <path d="M465,154.52c-.35-.32-.7-.66-1.05-1l.06,0C464.33,153.82,464.67,154.17,465,154.52Z" fill="#df2333" />
  <path d="M567.32,52.24c-.35-.32-.68-.64-1-.94l.05-.06Z" fill="#df2333" />
  <path d="M1035,276h276V0H1035Z" fill="#ffd2c7" />
  <path d="M1037,136.8l136.8,136.8V136.8Z" fill="#fff4f1" />
  <path d="M1173.79,0V136.8H1310.6Z" fill="#141e49" />
  <path d="M1173.79,136.8V273.6L1310.6,136.8Z" fill="#df2333" />
  <path d="M1173.79,0,1037,136.8h136.8Z" fill="#f8c44e" />
  <rect x="138" y="69" width="276" height="207" fill="#ff4846" />
  <rect x="138" width="276" height="69" fill="#ffd2c7" />
  <rect width="138" height="69" fill="#141e49" />
  <rect y="69" width="138" height="207" fill="#f8c44e" />
  <rect x="1173.01" y="964.99" width="412.99" height="138.73" fill="#141939" />
  <rect x="1173" y="828" width="412.99" height="138.73" fill="#f5bc1c" />
  <path d="M1491.47,966.77v.31H1586V873.42c-.68,0-1.36,0-2,0C1532.87,873.39,1491.47,915.2,1491.47,966.77Z"
    fill="#fbe7c9" />
  <path d="M1491.54,967.8c1.81,50,42.49,89.9,92.42,89.9l2,0V967.81Z" fill="#3a3759" />
  <rect x="1311" width="275" height="828" fill="#3c446a" />
  <path
    d="M1512.65,198.72a68.17,68.17,0,0,0-5-20.21,66.8,66.8,0,0,0-14-21.06A64.84,64.84,0,0,0,1473,143.23a63.81,63.81,0,0,0-50.6,0,64.73,64.73,0,0,0-20.66,14.22,66.8,66.8,0,0,0-14,21.06,67.87,67.87,0,0,0-5,20.21h-.28v492h130.43v-492Zm-11,480.79H1393.74V204.4c.28-30.41,24.49-55.15,54-55.15s53.69,24.74,54,55.15V679.51Z"
    fill="#f5bc1c" />
  <path d="M1501.68,204.74V679.51H1393.74V204.4c.28-30.41,24.49-55.15,54-55.15s53.69,24.74,54,55.15Z" fill="#fff" />
  <rect x="827.99" y="483" width="206.99" height="206.99" fill="#f5bc1c" />
  <rect x="932.23" y="587.23" width="102.77" height="102.77" fill="#141939" />
  <path d="M1035,690H932.23L1035,587.23Z" fill="#df2333" />
  <path d="M828,690H621V483H828Z" fill="#facdc0" />
  <path d="M828,484.49V690H622.49Z" fill="#df2333" />
  <rect x="695.65" y="557.88" width="59.17" height="58.72" fill="#141939" />
  <path
    d="M865.43,539.56h0a9.78,9.78,0,0,1,13.85,0h0a9.81,9.81,0,0,1,0,13.86h0a9.8,9.8,0,0,1-13.86,0h0a9.8,9.8,0,0,1,0-13.86Z"
    fill="#fef3f0" />
  <path
    d="M823.68,581.3h0a9.8,9.8,0,0,1,13.86,0h0a9.8,9.8,0,0,1,0,13.86h0a9.8,9.8,0,0,1-13.86,0h0a9.8,9.8,0,0,1,0-13.86Z"
    fill="#fef3f0" />
  <path
    d="M781.94,623.05h0a9.8,9.8,0,0,1,13.86,0h0a9.8,9.8,0,0,1,0,13.86h0a9.8,9.8,0,0,1-13.86,0h0a9.81,9.81,0,0,1,0-13.86Z"
    fill="#fef3f0" />
  <rect x="1035" y="483" width="276" height="69" fill="#df2333" />
  <rect y="276" width="69" height="552" fill="#facdc0" />
  <path d="M898,690h137v414H898Z" fill="#3c446a" />
  <rect x="276" y="276" width="69" height="207" fill="#f2c272" />
  <rect x="1035" y="827" width="138" height="276" fill="#fffbfa" />
  <rect x="1035" y="1093.13" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="1073.61" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="1054.09" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="1034.57" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="1015.04" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="995.52" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="976" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="955.14" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="935.61" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="916.09" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="896.57" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="877.05" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="857.52" width="138" height="9.87" fill="#3a3759" />
  <rect x="1035" y="838" width="138" height="9.87" fill="#3a3759" />
  <path d="M1035,1103V827h138" fill="#facdc0" />
  <path d="M621,483H207V690H621Z" fill="#df2333" />
  <rect x="207" y="663.68" width="414" height="13.3" fill="#ed7665" />
  <rect x="207" y="632.12" width="414" height="13.3" fill="#ed7665" />
  <rect x="207" y="602.3" width="414" height="13.3" fill="#ed7665" />
  <rect x="207" y="572.47" width="414" height="13.3" fill="#ed7665" />
  <rect x="207" y="542.65" width="414" height="13.3" fill="#ed7665" />
  <rect x="207" y="509.32" width="414" height="13.3" fill="#ed7665" />
  <rect x="207" y="483" width="414" height="13.3" fill="#ed7665" />
  <line x1="276" y1="897" x2="276" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <path d="M276,828H898v276H276Z" fill="#facdc0" />
  <line x1="276" y1="897" x2="897" y2="897" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="276" y1="1035" x2="897" y2="1035" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="345" y1="828" x2="345" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="414" y1="828" x2="414" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="483" y1="828" x2="483" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="552" y1="828" x2="552" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="621" y1="828" x2="621" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="690" y1="828" x2="690" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="759" y1="828" x2="759" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="828" y1="828" x2="828" y2="1104" fill="none" stroke="#fff" stroke-width="3" />
  <line x1="276" y1="966" x2="897" y2="966" fill="none" stroke="#fff" stroke-width="3" />
  <rect y="827.62" width="276.38" height="276.38" fill="#df2333" />
  <path d="M276.38,1049.62l-136.83,53.84-1.36.54L0,1049.62l38.21-60.78,101.34-161.2Z" fill="#ed7665" />
  <path d="M276.38,1049.62l-136.83,53.84V827.64Z" fill="#facdc0" />
  <rect x="69" y="276" width="207" height="207" fill="#3a3759" />
  <circle cx="172.5" cy="379.5" r="103.5" fill="#fff" />
  <circle cx="173" cy="380" r="72" fill="#f5bc1c" />
  <rect x="69" y="483" width="138" height="345" fill="#f5bc1c" />
  <rect x="897" width="138" height="276" fill="#f5bc1c" />
  <rect x="414" y="207" width="207" height="69" fill="#facdc0" />
  <text transform="translate(427.94 380)" font-size="40" fill="#3c446a"
    font-family="SourceSansPro-Bold, Source Sans Pro" font-weight="700" letter-spacing="0.08em"
    style="isolation: isolate">
    La
    <tspan x="59.56" y="0" letter-spacing="0.05em">c</tspan>
    <tspan x="80.36" y="0">oopé</tspan>
    <tspan x="181.36" y="0" letter-spacing="0.05em">r</tspan>
    <tspan x="199.56" y="0" letter-spacing="0.06em">a</tspan>
    <tspan x="223.16" y="0" letter-spacing="0.08em">ti</tspan>
    <tspan x="256.2" y="0" letter-spacing="0.07em">v</tspan>
    <tspan x="280.16" y="0">e d&apos;a</tspan>
    <tspan x="381.24" y="0" letter-spacing="0.07em">c</tspan>
    <tspan x="402.76" y="0" letter-spacing="0.08em">tivi</tspan>
    <tspan x="474.48" y="0" letter-spacing="0.06em">t</tspan>
    <tspan x="492.4" y="0" letter-spacing="0.08em">é Art &amp;</tspan>
    <tspan x="644.23" y="0" letter-spacing="0.07em">C</tspan>
    <tspan x="670.31" y="0">ultu</tspan>
    <tspan x="755.83" y="0" letter-spacing="0.07em">r</tspan>
    <tspan x="774.79" y="0" letter-spacing="0.08em">e</tspan>
  </text>
  <g style="isolation: isolate">
    <text transform="translate(272 764.4)" font-size="30" fill="#3c446a"
      font-family="SourceSansPro-Semibold, Source Sans Pro" font-weight="600" letter-spacing="0.05em"
      style="isolation: isolate">
      R
      <tspan x="19.44" y="0" letter-spacing="0.06em">A</tspan>
      <tspan x="38.04" y="0" letter-spacing="0.04em">S</tspan>
      <tspan x="55.74" y="0" letter-spacing="0.06em">SEMBLER · CRÉER · T</tspan>
      <tspan x="354.27" y="0">R</tspan>
      <tspan x="373.71" y="0" letter-spacing="0.06em">ANS</tspan>
      <tspan x="431.96" y="0" letter-spacing="0.05em">F</tspan>
      <tspan x="448.79" y="0" letter-spacing="0.06em">ORMER</tspan>
    </text>
  </g>
  <a href="http://google.fr">
    <rect x="1034.71" y="552" width="276.29" height="275" fill="#fff" />
  </a>
  <g style="isolation: isolate">
    <text transform="translate(1072.5 633.59)" font-size="30.22" fill="#3c446a"
      font-family="SourceSansPro-Bold, Source Sans Pro" font-weight="700" letter-spacing="0.08em"
      style="isolation: isolate">
      <tspan x="0" dy="1.2em">Annuaire&nbsp;des</tspan>
      <tspan x="0" dy="1.2em">coopérateurs</tspan>
    </text>
  </g>
  <path
    d="M1175,725.17,1142,716v45.56l32.3,9.09v.07H1220V725.18Zm-30,33.75V719.39l29.29,8.7v39.54Zm72,8.8h-39.74V728.17H1217Z"
    fill="#df2333" />
  <rect x="1199.98" y="734.53" width="11.77" height="2.99" fill="#df2333" />
  <rect x="1199.98" y="742.39" width="11.77" height="2.99" fill="#df2333" />
  <rect x="1199.98" y="750.24" width="11.77" height="2.99" fill="#df2333" />
  <rect x="1199.98" y="758.1" width="11.77" height="2.99" fill="#df2333" />
  <rect x="1182.57" y="734.79" width="11.77" height="2.99" fill="#df2333" />
  <rect x="1182.57" y="742.65" width="11.77" height="2.99" fill="#df2333" />
  <rect x="1182.57" y="750.51" width="11.77" height="2.99" fill="#df2333" />
  <rect x="1182.57" y="758.36" width="11.77" height="2.99" fill="#df2333" />
</svg>
