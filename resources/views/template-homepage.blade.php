{{--
  Template Name: Homepage Template
--}}

@extends('layouts.app')

@section('content')

@include('partials.content-cover')

@include('partials.sections.section-scrollslide', [
  'is_h1' => true,
  'section_data' => $page_fields['scroll_slide']
])

@include('partials.sections.section-bloc',
[
  'section_data' => $page_fields['cooperateurs'],
])

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.section-numbers', ['section_data' => $page_fields['stats']])

@include('partials.sections.spacer', ['desktop'=>70, 'mobile'=>30 ])

@include('partials.sections.section-metiers', ['footer' => true, 'lien_1' => @$page_fields['poles_metiers_lien_1'], 'lien_2' => @$page_fields['poles_metiers_lien_2']])

@include('partials.sections.spacer', ['desktop'=>0, 'mobile'=>30 ])

@include('partials.sections.section-blog')

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.section-contact')

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@if(isset($page_fields['categorie_partenaires']->name))
  @include('partials.sections.section-carrousel', [
  'class'=>'bg-white',
  'data' => get_partenaires($page_fields['categorie_partenaires']->term_id),
  'id'=>$page_fields['categorie_partenaires']->slug
  ])
@endif

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@endsection
