@extends('layouts.app')

@section('content')

  @if(isset($global_options['blog_titre']) && !empty($global_options['blog_titre']))
    @include('partials.page-header', ['title' => $global_options['blog_titre']])
  @endif

  <div class="container">

    <ul class="ul__categories">
      <li><a href="{{get_permalink( get_option( 'page_for_posts' ) )}}">{{$global_options['blog_view_all']}}</a></li>
      @if(is_array($categories))
        @foreach($categories as $categorie)
          @php
          $active = (get_queried_object()->term_id == $categorie->term_id);
          @endphp
          <li class="{{($active) ? 'active' : ''}}"><a href="{{get_term_link($categorie->term_id)}}">{{$categorie->name}}</a></li>
        @endforeach
      @endif
      {{-- <li><a href="#">Récits de coopérateurs</a></li>
      <li><a href="#">Projets collectifs</a></li>
      <li><a href="#">Ressources inspirantes </a></li>
      <li><a href="#">On parle de nous</a></li> --}}
    </ul>
    <div class="row articles">
      @foreach($posts as $post)
        <div class="col-12 col-md-4 article-item">
          @include('partials.cards.card-blog', ['data' => $post])
        </div>
      @endforeach
    </div>

    {!! the_posts_navigation(array('prev_text' => 'Plus récent', 'next_text' => 'Plus ancien')) !!}

  </div>


@endsection
