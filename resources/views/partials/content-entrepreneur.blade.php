<div class="gabarits-container">


  {{-- SAMPLE --}}
  {{-- <section class="entrepreneur entrepreneur__sample">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">
          left
        </div>
        <div class="right col-md-7 ">
          right
        </div>
      </div>
    </div>
  </section> --}}

  @if($data->type_header == 'image' && $data->cover_image > 0)

    {{-- cover cover-image --}}
    <div class="cover cover-image gs_reveal {{($data->cover_image_format == 'full') ? 'cover-image-full' : ''}}">
      <img
        src="{{wp_get_attachment_image_url( $data->cover_image, 'm'  )}}"
        srcset="{{wp_get_attachment_image_srcset( $data->cover_image )}}"
        sizes="100vw"
        alt="{{get_post_meta($data->cover_image, '_wp_attachment_image_alt', TRUE)}}">
    </div>
  @endif

  @if($data->type_header == 'video' && !empty($data->cover_video))

    {{-- cover cover-video --}}
    <div class="cover cover-video gs_reveal">
      <div class="embed-responsive embed-responsive-21by9">
        <video autoplay loop muted playsinline class="embed-responsive-item"
          poster="//via.placeholder.com/1280x720?text=poster-video">
          <source src="{{$data->cover_video}}" type="video/mp4">
        </video>
      </div>
    </div>
  @endif

  @if($data->type_header == 'slider' && sizeof($data->cover_slider) > 0)

    {{-- cover cover-slider --}}
    <div class="cover cover-slider gs_reveal">
      <div class="swiper swiper-entrepreneur-cover ">
        <div class="swiper-wrapper">
          @foreach($data->cover_slider as $slide)
          <div class="swiper-slide">
            <img
              src="{{wp_get_attachment_image_url( $slide , 'm'  )}}"
              srcset="{{wp_get_attachment_image_srcset( $slide )}}"
              sizes="100vw"
              alt="{{get_post_meta($slide, '_wp_attachment_image_alt', TRUE)}}">
          </div>
          @endforeach

        </div>

        {{-- @if(sizeof($data->cover_slider) > 1) --}}
          <div class="swiper-button-prev">
            @include('svg.arrow-left')
          </div>
          <div class="swiper-button-next">
            @include('svg.arrow-right')
          </div>
        {{-- @endif --}}
      </div>
    </div>
  @endif

  <div class="sections">
    @if($data->template == 'avance')
      @include('partials.sections.entrepreneur-complexe', ['data_section' => $data->premiere_section_avancee])
    @endif
    @if($data->template == 'simple')
      @include('partials.sections.entrepreneur-simple', ['data_section' => $data->premiere_section_simple])
    @endif

    @if(sizeof($data->sections) > 0)
      @include('partials.content-sections', ['sections' => $data->sections])
    @endif
  </div>
</div>
  {{-- <section class="entrepreneur entrepreneur__sample">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">

        </div>
        <div class="right col-md-7 ">

        </div>
      </div>
    </div>
  </section> --}}



