<div id="coverHomeDesktop" class="cover-container cover-desktop d-none d-lg-block">
  @include('svg.cover_desktop')
</div>

<div id="coverHomeMobile" class="cover-container cover-mobile d-lg-none">
  @include('svg.cover_mobile')
</div>
