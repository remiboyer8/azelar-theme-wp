<div class="gabarits-container">


  {{-- SAMPLE --}}
  {{-- SAMPLE --}}
  {{-- SAMPLE --}}
  {{-- <section class="entrepreneur entrepreneur__sample">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">
          left
        </div>
        <div class="right col-md-7 ">
          right
        </div>
      </div>
    </div>
  </section> --}}

  @if($data->type_header == 'image' && $data->cover_image > 0)
    {{-- cover cover-image --}}
    {{-- cover cover-image --}}
    {{-- cover cover-image --}}
    <div class="cover cover-image">
      <img
        src="{{wp_get_attachment_image_url( $data->cover_image, 'm'  )}}"
        srcset="{{wp_get_attachment_image_srcset( $data->cover_image )}}"
        sizes="{{wp_get_attachment_image_sizes( $data->cover_image )}}"
        alt="{{get_post_meta($data->cover_image, '_wp_attachment_image_alt', TRUE)}}">
    </div>
  @endif

  @if($data->type_header == 'video' && !empty($data->cover_video))
    {{-- cover cover-video --}}
    {{-- cover cover-video --}}
    {{-- cover cover-video --}}
    <div class="cover cover-video">
      <div class="embed-responsive embed-responsive-21by9">
        <video autoplay loop muted playsinline class="embed-responsive-item"
          poster="//via.placeholder.com/1280x720?text=poster-video">
          <source src="{{$data->cover_video}}" type="video/mp4">
        </video>
      </div>
    </div>
  @endif

  @if($data->type_header == 'slider' && sizeof($data->cover_slider) > 0)
    {{-- cover cover-slider --}}
    {{-- cover cover-slider --}}
    {{-- cover cover-slider --}}
    <div class="cover cover-slider">
      <div class="swiper swiper-entrepreneur-cover ">
        <div class="swiper-wrapper">
          @foreach($data->cover_slider as $slide)
          <div class="swiper-slide">
            <img
              src="{{wp_get_attachment_image_url( $slide , 'm'  )}}"
              srcset="{{wp_get_attachment_image_srcset( $slide )}}"
              sizes="{{wp_get_attachment_image_sizes( $slide )}}"
              alt="{{get_post_meta($slide, '_wp_attachment_image_alt', TRUE)}}">
          </div>
          @endforeach

        </div>
        @if(sizeof($data->cover_slider) > 1)
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>
        @endif
      </div>
    </div>
  @endif

  <section class="entrepreneur entrepreneur__maxi">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">
          <div class="text-1-wrapper">
            <p class="text-1 d-none d-md-block"><strong>Advenit post</strong> multos Scudilo Scutariorum tribunus
              velamento
              subagrestis ingenii persuasionis opifex callidus. qui eum
              adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
              flagrantibus
              votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum est remissurus ut mitis et
              clemens,
              participemque eum suae.</p>
          </div>
          <div class="dots d-none d-md-block"></div>
          <img src="http://via.placeholder.com/480x640" alt="" class="img-fluid img-portrait-1">
          <div class="text-2-wrapper">
            <p class="text-2"><strong>Lorem, ipsum dolor sit amet consectetur</strong> adipisicing elit. Libero numquam
              nihil non. Delectus
              magni sint
              neque, non consequatur ducimus maiores cum nihil a dolorum ex laboriosam voluptas culpa illo sapiente.</p>
          </div>
        </div>
        <div class="right col-md-7 ">
          <div class="text-1-wrapper">
            <p class="text-1 d-md-none"><strong>Advenit post</strong> multos Scudilo Scutariorum tribunus velamento
              subagrestis
              ingenii persuasionis opifex
              callidus. qui eum
              adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
              flagrantibus
              votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum est remissurus ut mitis et
              clemens,
              participemque eum suae.</p>
            <img src="http://via.placeholder.com/640x480" alt="" class="img-fluid img-landscape-1">
          </div>
          <div class="dots d-md-none"></div>
          <div class="text-3-wrapper">
            <h3 class="title">Advenit post multos Scudilo advenit</h3>
            <p class="text-3">texte 2 Lorem, ipsum dolor sit amet consectetur adipisicing elit. Libero numquam nihil
              non.
              Delectus magni
              sint
              neque, non consequatur ducimus maiores cum nihil a dolorum ex laboriosam voluptas culpa illo sapiente.</p>
          </div>
          <img src="http://via.placeholder.com/640x480" alt="" class="img-fluid img-landscape-2">
        </div>
      </div>
    </div>
  </section>

  <section class="entrepreneur entrepreneur__vimeo my-5">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">
          <div class="poster" data-toggle="modal" data-target="#modal-video" data-vimeo="601088734">
            <img src="http://via.placeholder.com/640x480?text=poster" alt="" class="img-fluid">
          </div>
        </div>
        <div class="right col-md-7 ">
          <div class="title">Advenit post multos Scudilo advenit suctilo.</div>
          <p>Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex
            callidus.
            qui eum
            adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
            flagrantibus
            votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum est remissurus ut mitis et
            clemens,
            participemque eum suae maiestatis</p>
        </div>
      </div>
    </div>
  </section>

  <section class="entrepreneur entrepreneur__audio my-5">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-8 col-xxxl-6 offset-xxxl-1">
          Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex callidus.
          qui eum
          adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
          flagrantibus
          votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum est remissurus ut mitis et clemens,
          participemque eum suae maiestatis
        </div>
        <div class="right col-md-4 col-xxxl-3">
          <script src="https://w.soundcloud.com/player/api.js"></script>
          <iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay"
            src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/45361390&color=%23aec4dc&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
            class="soundcloud"></iframe>
        </div>
      </div>
    </div>
  </section>



  <section class="entrepreneur entrepreneur__sample">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">

        </div>
        <div class="right col-md-7 ">

        </div>
      </div>
    </div>
  </section>


  <section class="entrepreneur entrepreneur__bloc_text">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">
          <div class="bloc">
            <strong>Advenit post</strong> multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis
            opifex callidus.
            qui eum
            adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
            flagrantibus
            votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum
          </div>
        </div>
        <div class="right col-md-7 ">
          <h3 class="title">Advenit post multos Scudilo advenit</h3>
          <p class="text">Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis
            opifex callidus. qui eum
            adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
            flagrantibus
            votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum est remissurus ut mitis et
            clemens,
            participemque eum suae maiestatis adscisceret, futurum laborum quoque socium, quos Arctoae provinciae diu
            fessae
            poscebant. Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis
            opifex
            callidus.
            qui eum adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius .</p>
        </div>
      </div>
      <div class="row">
        <div class="col-xxxl-10 offset-xxxl-1">
          <div class="dots"></div>
        </div>
      </div>
    </div>

  </section>

  <section class="entrepreneur entrepreneur__highlight_photo">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-8 col-xxxl-6 offset-xxxl-1">
          Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex callidus.
          qui
          eum
          adulabili sermonevelamento subagrestis ingenii persuasionis opifex
        </div>
        <div class="right col-md-4 col-xxxl-3 offset-xxxl-1">
          <img src="https://via.placeholder.com/345x345" alt="">
        </div>
      </div>
    </div>
  </section>


  <section class="entrepreneur entrepreneur__text_text ">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">
          Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex callidus.
          qui
          eum
          adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
          flagrantibus
          votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum est remissurus ut mitis et clemens,
          participemque eum suae maiestatis
          inprudentiam gestum est remissurus ut mitis et clemens, participemque eum suae maiestatis
        </div>
        <div class="right col-md-7 ">
          <div class="title">Advenit post multos Scudilo</div>
          <p>Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex
            callidus.
            qui eum
            adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando olus
            omnium
            proficisci pellexit vult s opifex callidus. qui eum adulabili sermone seriis admixto solus omnium proficisci
            pell.
            Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex</p>
        </div>
      </div>
    </div>
  </section>

  <section class="entrepreneur entrepreneur__img_text">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-7 col-xxxl-6 offset-xxxl-1">
          <img src="//via.placeholder.com/640x400" alt="" class="img-fluid">
        </div>
        <div class="right col-md-5 col-xxxl-3">
          Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex callidus.
          qui
          eum
          adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
          flagrantibus
          votis eum videre frater cuperet patruelis, siquid per inprudentiam gestum est remissurus ut mitis et clemens,
          participemque eum suae maiestatis adscisceret, futurum laborum quoque socium, quos Arctoae provinciae diu
          fessae
          poscebant. Advenit post multos Scudilo Scutariorum tribunus velamento.
        </div>
      </div>
    </div>
  </section>


  {{-- Les gabarits du blog --}}
  {{-- Les gabarits du blog --}}
  {{-- Les gabarits du blog --}}

  <section class="entrepreneur entrepreneur__title_text">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-8 col-xxxl-7 offset-xxxl-1">
          <h3 class="title">
            Advenit post multos Scudilo advenit suctilo.
          </h3>
          <p><strong>Advenit post multos Scudilo Scutariorum</strong> tribunus velamento subagrestis ingenii
            persuasionis
            opifex callidus.
            qui eum
            adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod
            flagrantibus
            votis eum videre frater cuperet patruelis adsimulato saepius lus omnium proficisci pellexit vultu adsimulato
            saepius
            replicando quod.</p>
        </div>
        <div class="right col-md-7 ">

        </div>
      </div>
    </div>
  </section>

  <section class="entrepreneur entrepreneur__dots">
    <div class="container ">
      <div class="row content">
        <div class="left col-12 col-xxxl-10 offset-xxxl-1">
          <div class="dots"></div>
        </div>
      </div>
    </div>
  </section>


  <section class="entrepreneur entrepreneur__text">
    <div class="container ">
      <div class="row content">
        <div class="left col-12 col-xxxl-10 offset-xxxl-1">
          <p>Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex
            callidus.
            qui eum
            adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando olus
            omnium
            proficisci pellexit vult s opifex callidus. qui eum adulabili sermone seriis admixto solus omnium proficisci
            pell.
            Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex.
            Soleo saepe ante oculos ponere, idque libenter crebris usurpare sermonibus, omnis nostrorum imperatorum,
            omnis
            exterarum
            gentium potentissimorumque populorum, omnis clarissimorum regum res gestas, cum tuis nec contentionum
            magnitudine nec
            numero proeliorum nec varietate regionum nec celeritate conficiendi nec dissimilitudine bellorum posse
            conferri; nec
            vero disiunctissimas terras citius passibus cuiusquam potuisse peragrari, quam tuis non dicam cursibus, sed
            victoriis
            lustratae sunt.
          </p>
          <p>Nihil morati post haec militares avidi saepe turbarum adorti sunt Montium primum, qui divertebat in
            proximo,
            levi
            corpore senem atque morbosum, et hirsutis resticulis cruribus eius innexis divaricaturn sine spiramento ullo
            ad usque
            praetorium traxere praefecti.
            votis eum videre frater cuperet patruelis adsimulato saepius lus omnium proficisci pellexit vultu adsimulato
            saepius replicando quod.</p>
        </div>

      </div>
    </div>
  </section>



</div>
