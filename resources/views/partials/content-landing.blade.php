<div id="landing" class="row no-gutters">
  <div class="left col-12 col-lg-7" style="background-image: url(@asset('images/landing-desktop.svg'))">
    <img src="@asset('images/landing-mobile.svg')" alt="" class="bg">
    <div style="background-image: url(@asset('images/landing-desktop@2x.png'))" class="logos_desktop"></div>
    <img src="@asset('images/landing-mobile@2x.png')" class="logos_mobile" />
  </div>

  <div class="right col-12 col-lg-5 scroller">
    <div class="wrapper">
      <div class="title">RASSEMBLER · CRÉER · TRANSFORMER</div>
      <div class="desc">LE SITE EST EN COURS DE CONSTRUCTION…<br><strong>Pour en savoir plus sur la première<br>coopérative d'activité art & culture de Rhône-Alpes,<br>n'hésitez pas à nous contacter !</strong></div>
      <div class="sep">
        <span></span>
        <span></span>
        <span></span>
      </div>
      <address>
        Azelar · Graines de SOL<br>
        122bis boulevard Emile Zola 69600 Oullins<br>
        <a href="tel:04 78 15 92 32">04 78 15 92 32</a> · <a href="mailto:contact@azelar.coop">contact@azelar.coop</a>
      </address>
      <div class="landing-form">
        <?php echo do_shortcode( '[contact-form-7 id="17" title="Formulaire contact (landing page)"]' ); ?>
      </div>
    </div>
  </div>
</div>

<div class="landing-footer">
  <div class="container">
    <span>@2021 · Azelar · Suivez-nous :</span>
    <div class="socials">
      <a href="https://www.instagram.com/azelar.coop/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
          <path d="M50,41.92A8.09,8.09,0,1,0,58.09,50,8.09,8.09,0,0,0,50,41.92Z" style="fill:#fff" />
          <path d="M68.91,35.7a7.65,7.65,0,0,0-1.81-2.79,7.52,7.52,0,0,0-2.79-1.81,13.53,13.53,0,0,0-4.5-.84c-2.56-.12-3.32-.14-9.8-.14s-7.24,0-9.8.14a13.21,13.21,0,0,0-4.5.84,7.65,7.65,0,0,0-2.79,1.81,7.52,7.52,0,0,0-1.81,2.79,13.53,13.53,0,0,0-.84,4.5c-.12,2.56-.14,3.33-.14,9.8s0,7.24.14,9.8a13.16,13.16,0,0,0,.84,4.5,7.65,7.65,0,0,0,1.81,2.79,7.52,7.52,0,0,0,2.79,1.81,13.47,13.47,0,0,0,4.5.84c2.56.12,3.32.14,9.8.14s7.24,0,9.8-.14a13.21,13.21,0,0,0,4.5-.84,7.65,7.65,0,0,0,2.79-1.81,7.52,7.52,0,0,0,1.81-2.79,13.47,13.47,0,0,0,.84-4.5c.12-2.56.14-3.32.14-9.8s0-7.24-.14-9.8A13.53,13.53,0,0,0,68.91,35.7ZM50,62.47A12.46,12.46,0,1,1,62.46,50,12.46,12.46,0,0,1,50,62.47ZM63,40a2.91,2.91,0,1,1,2.91-2.91A2.91,2.91,0,0,1,63,40Z" style="fill:#fff" />
          <path d="M85.36,14.65a50,50,0,1,0,0,70.71A50,50,0,0,0,85.36,14.65ZM74.11,60A17.85,17.85,0,0,1,73,65.9a12.41,12.41,0,0,1-7.1,7.1A17.85,17.85,0,0,1,60,74.13c-2.59.12-3.41.15-10,.15s-7.41,0-10-.15A17.85,17.85,0,0,1,34.1,73,12.41,12.41,0,0,1,27,65.9,17.85,17.85,0,0,1,25.87,60c-.12-2.59-.15-3.41-.15-10s0-7.41.15-10A17.85,17.85,0,0,1,27,34.12,12.41,12.41,0,0,1,34.1,27,17.85,17.85,0,0,1,40,25.89c2.59-.12,3.41-.15,10-.15s7.41,0,10,.15A17.91,17.91,0,0,1,65.88,27a12.41,12.41,0,0,1,7.1,7.1A17.85,17.85,0,0,1,74.11,40c.12,2.59.15,3.41.15,10S74.23,57.42,74.11,60Z" style="fill:#fff" />
        </svg>
      </a>

      <a href="https://www.facebook.com/Azelar.coop/" target="_blank">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
          <path d="M85.36,14.65a50,50,0,1,0,0,70.71A50,50,0,0,0,85.36,14.65Zm-23,18H56.51c-2.06,0-2.49.84-2.49,3v5.14h8.31l-.8,9H54v27H43.26V49.9h-5.6V40.76h5.6v-7.2c0-6.76,3.61-10.29,11.63-10.29h7.46v9.37Z" style="fill:#fff" />
        </svg>
      </a>

      <a href="https://www.linkedin.com/company/azelar/" target="_blank">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
          <path d="M85.36,14.65a50,50,0,1,0,0,70.71A50,50,0,0,0,85.36,14.65ZM39.86,72.2H30.7V42.62h9.16ZM35.24,38.74a5.46,5.46,0,1,1,5.41-5.46A5.44,5.44,0,0,1,35.24,38.74ZM74.17,72.2H65.06V56.67c0-4.26-1.62-6.64-5-6.64-3.67,0-5.58,2.48-5.58,6.64V72.2H45.72V42.62H54.5v4a10.31,10.31,0,0,1,8.91-4.89c6.27,0,10.76,3.83,10.76,11.75Z" style="fill:#fff" />
        </svg>
      </a>
    </div>
  </div>
</div>

@php

// the_content()

@endphp
