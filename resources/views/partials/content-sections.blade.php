
@foreach($sections as $section)
  @if($section->acf_fc_layout == 'video_texte')
    @include('partials.sections.entrepreneur-video_texte', ['data_section' => $section])
  @endif
  @if($section->acf_fc_layout == 'texte_audio')
    @include('partials.sections.entrepreneur-texte_audio', ['data_section' => $section])
  @endif
  @if($section->acf_fc_layout == 'photo_texte')
    @include('partials.sections.entrepreneur-photo_texte', ['data_section' => $section])
  @endif
  @if($section->acf_fc_layout == 'texte_texte')
    @include('partials.sections.entrepreneur-texte_texte', ['data_section' => $section])
  @endif
  @if($section->acf_fc_layout == 'texte_photo')
    @include('partials.sections.section-texte_photo', ['data_section' => $section])
  @endif
  @if($section->acf_fc_layout == 'texte')
    @include('partials.sections.texte', ['data_section' => $section])
  @endif
  @if($section->acf_fc_layout == 'spacer')
    @if(isset($section->desktop) && isset($section->mobile))
      @include('partials.sections.spacer', ['desktop'=>$section->desktop, 'mobile'=>$section->mobile ])
    @endif
  @endif
@endforeach
