<button class="navbar-toggler aside-navbar-toggler js-toggle-element-class" type="button" data-toggle="collapse"
  data-target="#aside-facets" data-class="js-aside-open" data-element="body" aria-controls="aside-facets"
  aria-expanded="true" aria-label="Toggle aside">
  <div class="wrapper">
    <span></span>
    <span></span>
    <span></span>
  </div>
</button>


{{-- @php
$parts = explode("/", $_SERVER['REQUEST_URI']);
$currentpole = $parts[count($parts) - 1];
@endphp --}}


<nav id="aside-facets" class="collapse aside-collapse show custom-scroll custom-scroll__red" data-scrollbar>
  <div class="aside-body">
    <div class="custom-control custom-radio bg-red  {{(!empty(get_queried_object()->term_id)) ? '' : 'custom-radio-active'}}">
      <input type="radio" id="radio[all]" name="radiometiers" class="custom-control-input" {{(!empty(get_queried_object()->term_id)) ? '' : 'checked'}}>
      <label class="custom-control-label" for="radio[all]">
        <a class="js-toggle-element-class" data-class="js-page-unload" data-element="body"
          href="{{get_permalink($global_options['annuaire_page'])}}">
          {!!$global_options['annuaire_all_poles_metiers']!!}
        </a>
        </label>
    </div>

    @foreach ($poles_metiers as $k=>$pole_metier)
    @php
    $get_queried_object = get_queried_object();
    $active = (isset($get_queried_object->term_id ) && $get_queried_object->term_id == $pole_metier['id']);
    @endphp
    <div class="custom-control custom-radio bg-{{$pole_metier['color']}} {{($active) ? 'custom-radio-active' : ''}}">
      <input type="radio" id="radio[{{$k+1}}]" name="radiometiers" class="custom-control-input"
        data-url="{{$pole_metier['url']}}" {{($active) ? 'checked' : 'dddd'}}>
      <label class="custom-control-label" for="radio[{{$k+1}}]">
        <a class="js-toggle-element-class" data-class="js-page-unload" data-element="body"
          href="{{$pole_metier['url']}}">{!!$pole_metier['nom']!!}</a>
      </label>
    </div>
    @endforeach
</nav>


