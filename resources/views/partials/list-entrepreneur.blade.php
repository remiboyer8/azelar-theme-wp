<section class="section section__search">
  <div class="container">
    <div class="selects row">
      <div class="col-md-6 pr-xxxl-5 select-skills-container">
        <select class="js-select-skills form-control search-form" id="id_skill" name="skills" multiple="multiple"
          data-placeholder="{{$global_options['annuaire_champ_metier']}}">
          @foreach($metiers as $metier)
          <option value="metier-{{$metier['id']}}" data-alt="{{$metier['alt']}}">{{$metier['nom']}}</option>
          @endforeach
        </select>
        <div id="dropdown-skills" class="select2-dropdown-container"></div>
      </div>
      <div class="col-md-6 pl-xxxl-5 select-person-container">
        <select class="js-select-person form-control search-form" id="id_person" name="person"
          data-placeholder="{{$global_options['annuaire_champ_entrepreneur']}}">
          @foreach($select_entrepreneurs as $entrepreneur)
          <option data-id="{{$entrepreneur['id']}}" value="{{$entrepreneur['url']}}">
            {!!$entrepreneur['nom']!!} {!!$entrepreneur['prenom']!!} {!!(!empty($entrepreneur['nom_commercial'])) ? '('.$entrepreneur['nom_commercial'].')' : ''!!}
          </option>
          @endforeach
        </select>
        <div id="dropdown-person" class="select2-dropdown-container"></div>
      </div>
    </div>
  </div>
</section>


<section class="section__list">
  <div class="container">
    <div class="row">
      <?php //dd($entrepreneurs); ?>
      @foreach($entrepreneurs as $entrepreneur)
      @php
        $classes = (isset($entrepreneur['metiers']) && is_array($entrepreneur['metiers']) ) ? 'metier-'.implode(' metier-', $entrepreneur['metiers']) : '';
      @endphp
      <div class="col-12 col-md-6 col-lg-4 card__person-container {{$classes}}" @if (isset($entrepreneur['metier']))
        data-metier="{{$entrepreneur['metier']}}" @endif>
        @include('partials.cards.card-person', ['data_card' => $entrepreneur])
      </div>
      @endforeach
    </div>
  </div>
</section>


{{-- <nav aria-label="Page navigation">
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">...</li>
    <li class="page-item"><a class="page-link" href="#">8</a></li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav> --}}
