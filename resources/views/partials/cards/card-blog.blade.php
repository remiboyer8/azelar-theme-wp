<div class="card card__blog gs_reveal">

  <a href="{{$data['url']}}" class="card-header">
    <img src="{{wp_get_attachment_image_url( $data['vignette'] , 's'  )}}" class="card-img-top"
      data-srcset="{{wp_get_attachment_image_srcset( $data['vignette'] )}}"
      sizes="{{wp_get_attachment_image_sizes($data['vignette'])}}"
      alt="{{get_post_meta($data['vignette'], '_wp_attachment_image_alt', TRUE)}}">
    {{-- <img src="@asset('images/datas/blog_thumb.jpg')" class="card-img-top" alt="azelar"> --}}
  </a>

  <div class="card-body">

    <a href="{{$data['url']}}">
      <h3 class="card-title">{!!$data['titre']!!}</h3>
    </a>
    <div class="card-tags">
      @if(isset($data['categories']) && is_array($data['categories']))
      @foreach($data['categories'] as $k=>$cat)
      <a href="{{$cat['url']}}">#{{sanitize_title($cat['title'])}}</a>{{(($k + 1) < sizeof($data['categories'])) ? ','
        : '' }} @endforeach @endif {{-- <a href="/">#categorie</a>,
        <a href="/">#categorie</a>,
        <a href="/">#categorie</a> --}}
    </div>

  </div>
  <div class="card-footer">
    <a href="https://www.addtoany.com/share" class="card-share a2a_dd" data-a2a-url="{{$data['url']}}"
      data-a2a-title="{{$data['titre']}}">
      @include('svg.share')
    </a>
    <a href="{{$data['url']}}" class="card-link">@include('svg.arrow-right')</a>
  </div>
</div>
