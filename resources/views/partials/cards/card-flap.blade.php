<div class="card card__flap
 js-toggle-class gs_reveal gs_reveal_fromLeft {{$class}}" data-class="open">
  <div class="recto">
    <div class="wrapper">
      {!!$recto!!}
    </div>
  </div>
  <div class="verso">
    <div class="wrapper">
      {!!$verso!!}
    </div>
  </div>
  <div class="handler">
    @if ($icon =='arrow' )
      @include('svg.arrow-right')
    @endif
    @if ($icon =='plus' )
      @include('svg.plus')
    @endif
  </div>
</div>
