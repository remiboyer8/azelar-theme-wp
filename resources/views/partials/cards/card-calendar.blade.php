<div class="card card__calendar gs_reveal gs_reveal_fromLeft">
  <img src="@asset('images/datas/rect.svg')" aria-hidden="true" class="calillus1 d-none d-md-block">

  @if(isset($global_options['contact_reunion_titre']))
    {{$global_options['contact_reunion_titre']}}
  @endif

  @if(isset($global_options['contact_reunion_titre']))
    <strong>{{$global_options['contact_reunion_date']}}</strong>
  @endif

  @if(isset($global_options['contact_reunion_adresse']))
    <address>{!!$global_options['contact_reunion_adresse']!!}</address>
  @endif

  @if(isset($global_options['contact_reunion_inscription_texte']) && isset($global_options['contact_telephone']))
    <small>
      {!!$global_options['contact_reunion_inscription_texte']!!}
      @if(isset($global_options['contact_telephone']))
        <a href="tel:{{str_replace(' ', '', $global_options['contact_telephone'])}}">{{$global_options['contact_telephone']}}</a>
      @endif
    </small>
  @endif

  <img src="@asset('images/datas/losange.svg')" aria-hidden="true" class="calillus2 d-md-none">
</div>
