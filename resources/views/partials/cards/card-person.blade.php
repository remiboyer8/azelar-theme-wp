<div id="card__person1" class="card card__person">
  <div class="card-wrapper">
    <a href="{{$data_card['url']}}" class="card-swiper">
      <div class="swiper swiper-basic" data-controls="#controls{{$data_card['id']}}">
        <div class="swiper-wrapper">
          @if(isset($data_card['listing_slider']) && is_array($data_card['listing_slider']) &&
          sizeof($data_card['listing_slider']) > 0)
          @foreach($data_card['listing_slider'] as $slide)
          <div class="swiper-slide">
            {{-- <img data-src="{{wp_get_attachment_image_url( $slide , 's'  )}}" class="swiper-lazy"
              data-srcset="{{wp_get_attachment_image_srcset( $slide )}}"
              sizes="{{wp_get_attachment_image_sizes($slide)}}"
              alt="{{get_post_meta($slide, '_wp_attachment_image_alt', TRUE)}}">
 --}}
              <img data-src="{{wp_get_attachment_image_url( $slide , 's'  )}}" class="swiper-lazy"
              alt="{{get_post_meta($slide, '_wp_attachment_image_alt', TRUE)}}">
          </div>
          @endforeach
          @endif
        </div>
      </div>
    </a>

    @if(isset($data_card['listing_slider']) && is_array($data_card['listing_slider']) &&
    sizeof($data_card['listing_slider']) > 1)
    <div class="swiper-controls" id="controls{{$data_card['id']}}" class="">
      <div class="swiper-button-prev"> @include('svg.arrow-left')</div>
      <div class="swiper-button-next"> @include('svg.arrow-right')</div>
    </div>
    @else
    <div class="swiper-controls d-none" id="controls{{$data_card['id']}}">
      <div class="swiper-button-prev"> @include('svg.arrow-left')</div>
      <div class="swiper-button-next"> @include('svg.arrow-right')</div>
    </div>
    @endif
    <div class="card-body">
      <a href="{{$data_card['url']}}" class="card-content">
        @if(isset($data_card['prenom']) && !empty($data_card['prenom']))
        <h2 class="card-title">{!!$data_card['prenom']!!} {!!$data_card['nom']!!}</h2>
        @endif
        @if(isset($data_card['nom_commercial']) && !empty($data_card['nom_commercial']))
        <h3 class="card-subtitle">{!!$data_card['nom_commercial']!!}</h3>
        @endif
        @if(isset($data_card['metier']) && !empty($data_card['metier']))
        <h4 class="card-subtitle">{!!$data_card['metier']!!}</h4>
        @endif
      </a>

      <div class="card-links">
        <a href="https://www.addtoany.com/share"
          class="card-share a2a_dd"
          data-a2a-url="{{$data_card['url']}}"
          data-a2a-title="{{$data_card['prenom']}} {{$data_card['nom']}}">
          @include('svg.share')
        </a>
        @if(isset($data_card['metier']) && !empty($data_card['metier']))
        <a href="mailto:{{$data_card['email']}}" class="card-mail">
          @include('svg.mail')
        </a>
        @endif
        <a href="{{$data_card['url']}}" class="card-page">
          @include('svg.arrow-right')
        </a>
      </div>
    </div>
  </div>

</div>
