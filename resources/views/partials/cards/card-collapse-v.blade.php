@php
  $id = sanitize_title($data_card['nom']);
  $class = (isset($data_card['color'])) ? $data_card['color'] : 'corail';
@endphp
<div class=" card__collapse card__collapse__v gs_reveal {{$class}}">
  <div class="card-header collapsed" id="heading{{$id}}" data-toggle="collapse" data-target="#collapse{{$id}}"
    aria-expanded="false" aria-controls="collapse{{$id}}">
    <h3 class="card-title">
      {!!$data_card['nom']!!}
      {{-- Pôle accueil<br>
      et administratif --}}
    </h3>
    <button class="btn btn-link btn-block text-left" type="button">
      @include('svg.plus')
      @include('svg.moins')
    </button>
  </div>

  <div id="collapse{{$id}}" class="collapse" aria-labelledby="heading{{$id}}" >
    {{-- data-parent="#accordionPole" --}}
    @if(isset($data_card['membres']) && is_array($data_card['membres']) &&
    sizeof($data_card['membres']) > 0)
      @foreach($data_card['membres'] as $k=>$membre)
        @include('partials.cards.card-team', ['membre' => $membre])
      @endforeach
    @endif
    {{-- @include('partials.cards.card-team')
    @include('partials.cards.card-team') --}}
  </div>
</div>
