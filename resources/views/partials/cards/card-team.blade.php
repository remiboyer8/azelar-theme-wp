<div class="card card__team  gs_reveal gs_reveal-fromRight">
  @if(isset($membre['image']) && $membre['image'] > 0)
  <img src="{{wp_get_attachment_image_url($membre['image'], 'm')}}" class="card-img-top" alt="{{$membre['nom']}}">
  @endif
  {{-- <img src="@asset('images/datas/team-azelar1.jpg')" class="card-img-top" alt="Anne-Laure Guidicelli"> --}}
  <div class="card-body">
    @if(isset($membre['nom']) && !empty($membre['nom']))
    <h5 class="card-title">{!!$membre['nom']!!}</h5>
    @endif
    @if(isset($membre['description']) && !empty($membre['description']))
    <div class="card-subtitle">{!!$membre['description']!!}</div>
    @endif
  </div>
</div>
<?php //var_dump($membre); ?>
