@if(isset($first_actus) && is_array($first_actus) && sizeof($first_actus) > 0)
<section class="section section__blog">
  <div class="container">
    @if(isset($page_fields['chroniques_titre']) && !empty($page_fields['chroniques_titre']))
    <div class="section-header gs_reveal">
      <h2 class="section-title section-title__inline">{!!$page_fields['chroniques_titre']!!}
        {{-- Les chroniques <span>d'<img src="@asset('images/sprite/azelar.svg')"
            alt="azelar"></span> --}}
      </h2>
    </div>
    @endif
    <!-- Slider main container -->
    <div class="section-body">
      <div class="swiper swiper-coverflow ">
        <div class="swiper-wrapper">
          @foreach($first_actus as $actu)
            <div class="swiper-slide">
              @include('partials.cards.card-blog', ['data' => $actu])
            </div>
          @endforeach
          {{-- <div class="swiper-slide">
            @include('partials.cards.card-blog')
          </div>
          <div class="swiper-slide">
            @include('partials.cards.card-blog')
          </div> --}}
        </div>
      </div>
    </div>
    @if(isset($page_fields['chroniques_btn_texte']) && !empty($page_fields['chroniques_btn_texte']))
    <div class="section-footer gs_reveal">
      <a href="{{get_permalink( get_option( 'page_for_posts' ) )}}" class="btn btn-primary">
        {!!$page_fields['chroniques_btn_texte']!!}
      </a>
    </div>
    @endif
  </div>
</section>
@endif
