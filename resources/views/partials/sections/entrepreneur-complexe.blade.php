@if(isset($data_section->texte_1))

<section class="entrepreneur entrepreneur__maxi">
  <div class="container ">
    <div class="row content">
      <div class="left col-md-5 gs_reveal gs_reveal_fromLeft">
        <div class="text-1-wrapper d-none d-md-block gs_reveal gs_reveal_fromLeft">
          <div class="text-1 ">
            {!! $data_section->texte_1 !!}
          </div>
        </div>
        <div class="dots d-none d-md-block gs_reveal gs_reveal_fromLeft"></div>
        <div class="img-portrait-1-container">
          <img src="{{wp_get_attachment_image_url( $data_section->image_2 , 'm'  )}}"
            srcset="{{wp_get_attachment_image_srcset( $data_section->image_2 )}}"
            sizes="(width:100vw) 600px"
            loading="lazy"
            alt="{{get_post_meta($data_section->image_2, '_wp_attachment_image_alt', TRUE)}}"
            class="img-fluid img-portrait-1 gs_reveal gs_reveal_fromLeft">
        </div>
        <div class="text-2-wrapper gs_reveal gs_reveal_fromLeft">
          <div class="text-2">
            {!! $data_section->texte_3 !!}
          </div>
        </div>
      </div>
      <div class="right col-md-7  gs_reveal gs_reveal_fromRight">
        <div class="text-1-wrapper gs_reveal gs_reveal_fromRight">
          <div class="text-1 d-md-none">
            {!! $data_section->texte_1 !!}
          </div>
          <img src="{{wp_get_attachment_image_url( $data_section->image_1 , 'm'  )}}"
            srcset="{{wp_get_attachment_image_srcset( $data_section->image_1 )}}"
            sizes="(width:100vw) 700px"
            loading="lazy"
            alt="{{get_post_meta($data_section->image_1, '_wp_attachment_image_alt', TRUE)}}"
            class="img-fluid img-landscape-1 gs_reveal gs_reveal_fromRight">
        </div>
        <div class="dots d-md-none gs_reveal gs_reveal_fromRight"></div>
        <div class="text-3-wrapper">
          <h3 class="title gs_reveal gs_reveal_fromRight">{{ $data_section->titre_2 }}</h3>
          <div class="text-3 gs_reveal gs_reveal_fromRight">
            {!! $data_section->texte_2 !!}
          </div>
        </div>
        <img src="{{wp_get_attachment_image_url( $data_section->image_3 , 'm'  )}}"
          srcset="{{wp_get_attachment_image_srcset( $data_section->image_3 )}}"
          sizes="(width:100vw) 700px"
          loading="lazy"
          alt="{{get_post_meta($data_section->image_3, '_wp_attachment_image_alt', TRUE)}}"
          class="img-fluid img-landscape-2 gs_reveal gs_reveal_fromRight">
      </div>
    </div>
  </div>
</section>

@endif
