<section class="section section__team section__team__gds">
  <div class="container">
    @if(isset($section_data['titre']) && !empty($section_data['titre']))
    <div class="section-header gs_reveal">
      <h2 class="section-title">
        {!!$section_data['titre']!!}
        {{-- Ils travaillent au sein de l'équipe <img src="@asset('images/datas/graines-de-sol-blue.svg')"
          alt="graines de sol" style="vertical-align: middle"> --}}
      </h2>
    </div>
    @endif
    @if(isset($section_data['poles']) && is_array($section_data['poles']) &&
    sizeof($section_data['poles']) > 0)
    <div class="section-body row">
      <div class="col-10 offset-1 col-md-4 offset-md-4">
        {{-- <div class="accordion" id="accordionPole"> --}}
        <div class="accordion" id="accordionPole">
          @foreach($section_data['poles'] as $k=>$pole)
            @include('partials.cards.card-collapse-v', ['data_card' => $pole])
          @endforeach
          {{-- @include('partials.cards.card-collapse-v', ['id'=>'admin','class'=>'corail'])
          @include('partials.cards.card-collapse-v', ['id'=>'compta','class'=>'yellow'])
          @include('partials.cards.card-collapse-v', ['id'=>'accompagnement','class'=>'blue'])
          @include('partials.cards.card-collapse-v', ['id'=>'amorcage','class'=>'red']) --}}
        </div>
      {{-- </div> --}}
    </div>
    @endif
  </div>
</section>
