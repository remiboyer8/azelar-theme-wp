
<?php //var_dump($section_data); ?>
<section class="section section__tags">
  <div class="container">
    @if(isset($section_data['titre']) && !empty($section_data['titre']))
    <div class="section-header gs_reveal">
      <h2 class="section-title">
        {!!$section_data['titre']!!}
      </h2>
    </div>
    @endif
    <div class="section-body">
      <ul class="tags">
        @if(isset($section_data['tags']) && is_array($section_data['tags']) && sizeof($section_data['tags']) > 0)
          @foreach($section_data['tags'] as $ligne_tags)
          @php $tags = explode(PHP_EOL, $ligne_tags['ligne']); @endphp
          <li>
            @if(is_array($tags) && sizeof($tags) > 0)
              @foreach($tags as $tag)
                @if(!empty($tag))
                  <h4 class="gs_reveal">{!!$tag!!}</h4>
                @endif
              @endforeach
            @endif
          </li>
          @endforeach
        @endif
        {{-- <li>
          <h4 class="gs_reveal">Activités de création</h4>
        </li>
        <li>
          <h4 class="gs_reveal">Production</h4>
          <h4 class="gs_reveal">Administration</h4>
          <h4 class="gs_reveal">Diffusion</h4>
        </li>
        <li>
          <h4 class="gs_reveal">Accompagnement</h4>
          <h4 class="gs_reveal">Conseil</h4>
          <h4 class="gs_reveal">Formation</h4>
        </li>
        <li>
          <h4 class="gs_reveal">Intervention artistique</h4>
        </li>
        <li>
          <h4 class="gs_reveal">Direction technique</h4>
          <h4 class="gs_reveal">Régie</h4>
        </li> --}}
      </ul>
    </div>

    @if(isset($section_data['lien']['title']) && !empty($section_data['lien']['title']))
    <div class="section-footer gs_reveal">
      <a href="{{$section_data['lien']['url']}}" class="btn btn-annuaire btn-primary" target="{{$section_data['lien']['target']}}">
        @include('svg.annuaire')
        <span>{{$section_data['lien']['title']}}</span>
      </a>
    </div>
    @endif
  </div>
</section>
