<section class="section section__contact">
  <div class="container">
    @if(isset($global_options['contact_titre']))
    <div class="section-header gs_reveal">
      <{{(isset($is_h1) && $is_h1) ? 'h1' : 'h2'}} class="section-title">
        {{$global_options['contact_titre']}}
      </{{(isset($is_h1) && $is_h1) ? 'h1' : 'h2'}}>
    </div>
    @endif
    <div class="section-body row">

      <div class="col-12 col-xl-6 col-xxl-5 section-left">
        <div class="coords gs_reveal gs_reveal_fromLeft">
          <img src="@asset('images/sprite/azelar.svg')" alt="azelar" class="logo">
          <address class="icon-text">
            <div class="icon">
              <img src="@asset('images/sprite/maison.svg')" alt="siege social">
            </div>
            <div class="text">
              @if(isset($global_options['contact_adresse']))
              {!!$global_options['contact_adresse']!!}
              @endif
            </div>
          </address>
          @if(isset($global_options['contact_telephone']))
          <div class="links icon-text">
            <div class="icon">
            </div>
            <div class="text">
              <a
                href="tel:{{str_replace(' ', '', $global_options['contact_telephone'])}}">{{$global_options['contact_telephone']}}</a>
            </div>
          </div>
          @endif
          @if(isset($global_options['contact_email']))
          <div class="links icon-text">
            <div class="icon">
              <img src="@asset('images/sprite/mail-red.svg')" alt="siege social">
            </div>
            <div class="text">
              <a href="mailto:{{$global_options['contact_email']}}">{{$global_options['contact_email']}}</a>
            </div>
          </div>
          @endif
        </div>
        @include('partials.cards.card-calendar')
      </div>
      <div class="col-12 col-xl-6 col-xxl-7 section-right gs_reveal gs_reveal_fromRight">
        <div class="wrapper ">
          <img src="@asset('images/datas/losange.svg')" aria-hidden="true" class="illus2 d-none d-xl-block">
          @if(isset($global_options['contact_form_shortcode']))
          <?php echo do_shortcode( $global_options['contact_form_shortcode'] ); ?>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
