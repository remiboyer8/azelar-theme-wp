<?php //var_dump($section_data); ?>
@if(isset($section_data['display']) && $section_data['display'])
<section class="section section__testimony">

  @if(isset($section_data['titre']) && !empty($section_data['titre']))
  @include('partials.sections.title',
  ['title' => $section_data['titre']])
  @endif
  <div class="container">
    <div class="row">
      @if(isset($section_data['temoignages']) && is_array($section_data['temoignages']) &&
      sizeof($section_data['temoignages']) > 0)
      @foreach($section_data['temoignages'] as $k=>$temoignage)
      <div class="col-12 col-md-6 {{(($k+1) % 2 == 0) ? 'right' : 'left'}}">
        <div class="bloc  gs_reveal gs_reveal_from{{(($k+1) % 2 == 0) ? 'Right' : 'Left'}}">
          <div class="bloc-header ">
            <div class="portrait">
              @if(isset($temoignage['image']) && $temoignage['image'] > 0)
              <img src="{{wp_get_attachment_image_url($temoignage['image'], 's')}}" alt="{{$temoignage['auteur']}}">
              @endif
            </div>
            <div class="illus">
              @if(($k+1) % 2 == 0)
              <img src="@asset('images/datas/illus4.svg')" aria-hidden="true">
              @else
              <img src="@asset('images/datas/illus3.svg')" aria-hidden="true">
              @endif
            </div>
          </div>
          <div class="bloc-content">
            <div class="bloc-body">
              <div class="text">{!!$temoignage['texte']!!}</div>
            </div>
            <div class="bloc-footer">
              <a class="author" href="{{$temoignage['lien']}}">
                {!!$temoignage['auteur']!!}
              </a>
            </div>
          </div>
        </div>
      </div>
      @endforeach
      @endif
    </div>
    {{-- <div class="row">
      @if(isset($section_data['lien']) && !empty($section_data['lien']))
      <div class="col-12 col-md-6">
        <a href="{{$section_data['lien']['url']}}" class="btn btn-primary" target="{{$section_data['lien']['target']}}">
          <span>{{$section_data['lien']['title']}}</span>
        </a>
      </div>
      @endif
    </div> --}}
  </div>
</section>
@endif
