
<?php //var_dump($section_data); ?>
<section class="section section__team section__team__azelar">
  <div class="container">
    @if(isset($section_data['titre']) && !empty($section_data['titre']))
    <div class="section-header gs_reveal">
      <h2 class="section-title">
        {!!$section_data['titre']!!}
        {{-- Elles se dédient au pôle <img src="@asset('images/sprite/azelar.svg')" alt="azelar"> --}}
      </h2>
    </div>
    @endif
    @if(isset($section_data['membres']) && is_array($section_data['membres']) &&
    sizeof($section_data['membres']) > 0)
    <div class="section-body">
      <div class="row">
        @foreach($section_data['membres'] as $k=>$membre)
        <div class="col-12 col-md-4">
          @include('partials.cards.card-team', ['membre' => $membre])
        </div>
        @endforeach
        {{-- <div class="col-12 col-md-4">
          @include('partials.cards.card-team')
        </div>
        <div class="col-12 col-md-4">
          @include('partials.cards.card-team')
        </div>
        <div class="col-12 col-md-4">
          @include('partials.cards.card-team')
        </div> --}}
      </div>
    </div>
    @endif
  </div>
</section>
