
  <section class="entrepreneur entrepreneur__img_text">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-7 gs_reveal gs_reveal_fromLeft">
          <img
            src="{{wp_get_attachment_image_url( $data_section->image , 'm'  )}}"
            srcset="{{wp_get_attachment_image_srcset( $data_section->image )}}"
            sizes="(width:100vw) 700px"
            loading="lazy"
            alt="{{get_post_meta($data_section->image, '_wp_attachment_image_alt', TRUE)}}"
            class="img-fluid">
        </div>
        <div class="right col-md-5 gs_reveal gs_reveal_fromRight">
          {!! $data_section->texte !!}
        </div>
      </div>
    </div>
  </section>
