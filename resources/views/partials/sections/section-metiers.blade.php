<section class="section section__metiers">
  <div class="container">
    <?php //var_dump($global_options['poles_metiers']); ?>
    @if(isset($global_options['poles_metiers']['poles_metiers']) && is_array($global_options['poles_metiers']['poles_metiers']))
    <div class="row section-body">
      @foreach($global_options['poles_metiers']['poles_metiers'] as $pole_metier)
        @if(isset($pole_metier['url']) && isset($pole_metier['title']) && isset($pole_metier['picto']))
        <div class="card-wrapper col-6 col-md-4">
          <a href="{{$pole_metier['url']}}" class="card card__metier gs_reveal">
            <div class="card-header">
              @include('svg.'.$pole_metier['picto'])
            </div>
            <div class="card-body">
              <h3 class="card-title">{!!$pole_metier['title']!!}</h3>

              <div class="card-hover">
                {{-- Titre --}}
                <div class="card-hover-header">
                <strong>{!!$pole_metier['title']!!}</strong>
                  @if(isset($pole_metier['resume']) && !empty($pole_metier['resume']))
                  {{-- Description --}}
                    <span>{!!$pole_metier['resume']!!}</span>
                  @endif
                  </div>
                @if(isset($global_options['poles_metiers']['texte_hover']) && !empty($global_options['poles_metiers']['texte_hover']))
                {{-- Bouton Explorez les talents --}}
                <div class="card-hover-footer">
                  <span>{!!$global_options['poles_metiers']['texte_hover']!!}</span>
                </div>
                @endif
              </div>

            </div>
          </a>
        </div>
        @endif
        <?php //exit; ?>
      @endforeach
    </div>
    @endif
    {{-- <div class="row section-body">
      <div class="card-wrapper col-6 col-md-4">
        <a href="#" class="card card__metier gs_reveal">
          <div class="card-header">
            @include('svg.arts-appliques')
          </div>
          <div class="card-body">
            <h3 class="card-title">arts appliqués</h3>
            <div class="card-hover"><span>explorer les talents</span></div>
          </div>
        </a>
      </div>
      <div class="card-wrapper col-6 col-md-4">
        <a href="#" class="card card__metier gs_reveal">
          <div class="card-header">
            @include('svg.arts-visuels')
          </div>
          <div class="card-body">
            <h3 class="card-title">Arts visuels</h3>
            <div class="card-hover"><span>explorer les talents</span></div>
          </div>
        </a>
      </div>
      <div class="card-wrapper col-6 col-md-4">
        <a href="#" class="card card__metier gs_reveal">
          <div class="card-header">
            @include('svg.audiovisuel')
          </div>
          <div class="card-body">
            <h3 class="card-title">audiovisuel et&nbsp;numérique</h3>
            <div class="card-hover"><span>explorer les talents</span></div>
          </div>
        </a>
      </div>
      <div class="card-wrapper col-6 col-md-4">
        <a href="#" class="card card__metier gs_reveal">
          <div class="card-header">
            @include('svg.edition')
          </div>

          <div class="card-body">
            <h3 class="card-title">écriture et&nbsp;édition</h3>
            <div class="card-hover"><span>explorer les talents</span></div>
          </div>
        </a>
      </div>
      <div class="card-wrapper col-6 col-md-4">
        <a href="#" class="card card__metier gs_reveal">
          <div class="card-header">
            @include('svg.metiers-dart')
          </div>
          <div class="card-body">
            <h3 class="card-title">métiers d'art</h3>
            <div class="card-hover"><span>explorer les talents</span></div>
          </div>
        </a>
      </div>
      <div class="card-wrapper col-6 col-md-4">
        <a href="#" class="card card__metier gs_reveal">
          <div class="card-header">
            @include('svg.spectacle')
          </div>

          <div class="card-body">
            <h3 class="card-title">spectacle vivant</h3>
            <div class="card-hover"><span>explorer les talents</span></div>
          </div>
        </a>
      </div> --}}

    @if(isset($footer) && $footer)
    <div class="section-footer gs_reveal">
      <div class="row">

        @if(isset($lien_1['title']) && !empty($lien_1['title']))
        <div class="col-12 col-md-6 d-none d-md-block">
          <a href="{{$lien_1['url']}}" class="btn btn-annuaire btn-primary" target="{{$lien_1['target']}}">
            @include('svg.annuaire')
            <span>{{$lien_1['title']}}</span>
          </a>
        </div>
        @endif
        @if(isset($lien_2['title']) && !empty($lien_2['title']))
        <div class="col-12 col-md-6">
          <a href="{{$lien_2['url']}}" class="btn btn-primary" target="{{$lien_2['target']}}">
            <span>{{$lien_2['title']}}</span>
          </a>
        </div>
        @endif
      </div>
    </div>
    @endif
  </div>
</section>
