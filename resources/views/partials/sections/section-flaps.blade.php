<section class="section section__flaps">

    @if(isset($section_data['titre']) && !empty($section_data['titre']))
      @include('partials.sections.title', ['title' => $section_data['titre']])
    @endif

  <div class="section-body maxw-900">
    @if(isset($section_data['volets']) && is_array($section_data['volets']) && sizeof($section_data['volets']) > 0)
      @foreach($section_data['volets'] as $k=>$volet)
        @include('partials.cards.card-flap',[
        'icon'=>'arrow',
        'class'=>$volet['couleur'],
        'recto'=>$volet['texte'],
        'verso'=>$volet['texte_cache']
        ])
      @endforeach
    @endif
    {{-- @include('partials.cards.card-flap',[
    'icon'=>'arrow',
    'class'=>'corail',
    'recto'=>'Un statut d’entrepreneur-salarié qui conjugue
    <strong>liberté de l’indépendant</strong> et <strong>protection du régime
      salarié.</strong>',
    'verso'=>' <strong>Lorem ipsum dolor sit amet consectetur</strong> adipisicing elit. Cum culpa qui saepe odio
    similique
    inventore
    voluptatibus non sapiente vitae suscipit ducimus est aut, ut ullam, fugiat modi enim eum facere?'
    ])
    @include('partials.cards.card-flap',[
    'icon'=>'arrow',
    'class'=>'yellow',
    'recto'=>'Un cadre entrepreneurial simplifié qui
    <strong>allège les démarches administratives.</strong>',
    'verso'=>' <strong>Lorem ipsum dolor sit amet consectetur</strong> adipisicing elit. Cum culpa qui saepe odio
    similique
    inventore
    voluptatibus non sapiente vitae suscipit ducimus est aut, ut ullam, fugiat modi enim eum facere?'
    ])
    @include('partials.cards.card-flap',[
    'icon'=>'arrow',
    'class'=>'blue',
    'recto'=>'<strong>Une entreprise partagée</strong> qui favorise les dynamiques de réseaux, la mutualisation et les
    collaborations.',
    'verso'=>' <strong>Lorem ipsum dolor sit amet consectetur</strong> adipisicing elit. Cum culpa qui saepe odio
    similique
    inventore
    voluptatibus non sapiente vitae suscipit ducimus est aut, ut ullam, fugiat modi enim eum facere?'
    ])
    @include('partials.cards.card-flap',[
    'icon'=>'arrow',
    'class'=>'red',
    'recto'=>'<strong>Un accompagnement individuel et collectif</strong> pour monter en compétences sur tous les aspects
    de son activité.',
    'verso'=>' <strong>Lorem ipsum dolor sit amet consectetur</strong> adipisicing elit. Cum culpa qui saepe odio
    similique
    inventore
    voluptatibus non sapiente vitae suscipit ducimus est aut, ut ullam, fugiat modi enim eum facere?'
    ])
    @include('partials.cards.card-flap',[
    'icon'=>'arrow',
    'class'=>'corail',
    'recto'=>'Des fonctions supports pour se consacrer sereinement à <strong>son cœur de métier.</strong>',
    'verso'=>' <strong>Lorem ipsum dolor sit amet consectetur</strong> adipisicing elit. Cum culpa qui saepe odio
    similique
    inventore
    voluptatibus non sapiente vitae suscipit ducimus est aut, ut ullam, fugiat modi enim eum facere?'
    ]) --}}
  </div>

  @if(isset($section_data['lien']['title']) && !empty($section_data['lien']['title']))
    <div class="section-footer gs_reveal">
      <a href="{{$section_data['lien']['url']}}" class="btn btn-primary" target="{{$section_data['lien']['target']}}">
        <span>{{$section_data['lien']['title']}}</span>
      </a>
    </div>
  @endif
</section>
