<section class="section-spacer">
  <div style="padding-top: {{$desktop}}px" class="d-none d-md-block"></div>
  <div style="padding-top: {{$mobile}}px" class="d-md-none"></div>
</section>
