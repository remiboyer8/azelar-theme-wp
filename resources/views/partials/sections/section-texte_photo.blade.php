<section class="section section__highlight_photo">
  <div class="container">
    <div class="row content">
      @if(wp_get_attachment_image_url( $data_section->image, 'm') !== false)
      <div class="left col-md-8 gs_reveal gs_reveal_fromLeft">
        {!! $data_section->texte !!}
      </div>
      <div class="right col-md-4 gs_reveal gs_reveal_fromRight">
        <img class="img-fluid" src="{{wp_get_attachment_image_url( $data_section->image , 'm'  )}}"
          srcset="{{wp_get_attachment_image_srcset( $data_section->image )}}"
          sizes="(width:100vw) 500px"
          alt="{{get_post_meta($data_section->image, '_wp_attachment_image_alt', TRUE)}}">
      </div>
      @else
      <div class="left col-md-10 offset-md-1 gs_reveal text-center">
        {!! $data_section->texte !!}
      </div>
      @endif
    </div>
  </div>
</section>
