<section class="section section__two-blocs">

  @if(isset($section_data['titre']) && !empty($section_data['titre']))
  @include('partials.sections.title',
  ['title' => $section_data['titre']])
  @endif
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 bloc-container">
        <div class="bloc gs_reveal gs_reveal_fromLeft bg-yellow">
          @if(isset($section_data['titre_1']) && !empty($section_data['titre_1']))
          <div class="title">{!!$section_data['titre_1']!!}</div>
          @endif
          @if(isset($section_data['texte_1']) && !empty($section_data['texte_1']))
          <div class="text">{!!$section_data['texte_1']!!}</div>
          @endif
        </div>
      </div>
      <div class="col-12 col-md-6 bloc-container">
        <div class="bloc gs_reveal gs_reveal_fromRight bg-red-light">
          @if(isset($section_data['titre_2']) && !empty($section_data['titre_2']))
          <div class="title">{!!$section_data['titre_2']!!}</div>
          @endif
          @if(isset($section_data['texte_2']) && !empty($section_data['texte_2']))
          <div class="text">{!!$section_data['texte_2']!!}</div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
