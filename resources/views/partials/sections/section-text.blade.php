<section class="section section__text gs_reveal">

  @if(isset($section_data['titre']) && !empty($section_data['titre']))
  @include('partials.sections.title',
  ['title'=>$section_data['titre']])
  @endif
  <div class="container">
    <div class="row section-body gs_reveal">
      <div class="col-12 col-xl-10 offset-xl-1">
        @if(isset($section_data['texte']) && !empty($section_data['texte']))
        {!!$section_data['texte']!!}
        @endif
        {{-- wysiwig --}}
        {{-- <em>Nous croyons aux approches plurielles et transversales comme source de richesse et d’ouverture.
        </em><br>
        <p>La <strong>construction collective</strong> ouvre des espaces décloisonnés d’expérimentation et de
          réalisation. La multi-activité et la pluridisciplinarité trouvent naturellement leur place pour que
          différentes pratiques et sensibilités se rencontrent.</p> --}}
      </div>
    </div>
    @if(isset($section_data['texte_footer']) && !empty($section_data['texte_footer']))
    <div class="section-footer gs_reveal">
      {!!$section_data['texte_footer']!!}
      {{-- Notre organisation porte les valeurs de liberté, d’indépendance et de bien commun, pour que l’art et la
      culture
      demeurent source d’audace et d’expression. --}}
    </div>
    @endif
  </div>
</section>
