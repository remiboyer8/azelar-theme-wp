
@if(isset($section_data['lien']['title']) && !empty($section_data['lien']['title']))
  <div class="text-center">
    <a href="{{$section_data['lien']['url']}}"
      class="btn btn-primary {{isset($section_data['type_bouton']) ? $section_data['type_bouton'] : ''}}"
      target="{{isset($section_data['lien']['target']) ? $section_data['lien']['target'] : ''}}">
      @if(isset($section_data['icon']) && !empty($section_data['icon']))
        <span>{{$section_data['icon']}}</span>
      @endif
      <span>{{$section_data['lien']['title']}}</span>
    </a>
  </div>
@endif
