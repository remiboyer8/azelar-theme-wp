

  <section class="entrepreneur entrepreneur__audio">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 ">
          @if(isset($data_section->titre) && !empty($data_section->titre))
          <div class="title gs_reveal gs_reveal_fromLeft">{!! $data_section->titre !!}</div>
          @endif
          @if(isset($data_section->texte) && !empty($data_section->texte))
          <div class="text gs_reveal gs_reveal_fromLeft">{!! $data_section->texte !!}</div>
          @endif
        </div>
        @if(isset($data_section->soundcloud) && !empty($data_section->soundcloud))
        <div class="right col-md-7 gs_reveal gs_reveal_fromRight">
          {!!$data_section->soundcloud!!}
          {{-- <script src="https://w.soundcloud.com/player/api.js"></script>
          <iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay"
            src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{$data_section->id_soundcloud}}&color=%23aec4dc&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
            class="soundcloud"></iframe> --}}
        </div>
        @endif
      </div>
    </div>
  </section>
