
  <section class="entrepreneur entrepreneur__text_text">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 gs_reveal gs_reveal_fromLeft">
          {!! $data_section->texte_1 !!}
        </div>
        <div class="right col-md-7 gs_reveal gs_reveal_fromRight">
          <div class="title">{{$data_section->titre_2}}</div>
          <div>
            {!! $data_section->texte_2 !!}
          </div>
        </div>
      </div>
    </div>
  </section>
