<section class="section section__joinus">
  <div class="container">
    @if(isset($section_data['titre']) && !empty($section_data['titre']))
    <div class="section-header gs_reveal">
      <h2 class="section-title section-title__inline">
          {!!$section_data['titre']!!}
        {{-- Envie d’intégrer <img src="@asset('/images/sprite/azelar.svg')" alt="azelar">?
        <br>Rejoignez-nous en 3 étapes ! --}}
      </h2>
    </div>
    @endif
    <div class="section-body gs_reveal">
      <div class="steps">
        @if(isset($section_data['titre_1']) && !empty($section_data['titre_1']))
        <div class="row step">
          <div class="col-12 col-md-2 section-left">
            <span class="badge">1</span>
          </div>
          <div class="col-12 col-md-10 section-right">
            <div class="title">{!!$section_data['titre_1']!!}</div>
            @if(isset($section_data['texte_1']) && !empty($section_data['texte_1']))
            <div class="text">{!!$section_data['texte_1']!!}</div>
            @endif

            @include ('partials.cards.card-calendar')
          </div>
        </div>
        @endif
        @if(isset($section_data['titre_2']) && !empty($section_data['titre_2']))
        <div class="row step">
          <div class="col-12 col-md-2 section-left">
            <span class="badge">2</span>
          </div>
          <div class="col-12 col-md-10 section-right">
            <div class="title">{!!$section_data['titre_2']!!}</div>
            @if(isset($section_data['texte_2']) && !empty($section_data['texte_2']))
            <div class="text">{!!$section_data['texte_2']!!}</div>
            @endif
          </div>
        </div>
        @endif
        @if(isset($section_data['titre_3']) && !empty($section_data['titre_3']))
        <div class="row step">
          <div class="col-12 col-md-2 section-left">
            <span class="badge">3</span>
          </div>
          <div class="col-12 col-md-10 section-right">
            <div class="title">{!!$section_data['titre_3']!!}</div>
            @if(isset($section_data['texte_3']) && !empty($section_data['texte_3']))
            <div class="text">{!!$section_data['texte_3']!!}</div>
            @endif
          </div>
        </div>
        @endif
      </div>
    </div>
    @if(isset($section_data['lien']['title']) && !empty($section_data['lien']['title']))
      <div class="section-footer">
        <a href="{{$section_data['lien']['url']}}" class="btn btn-secondary" target="{{$section_data['lien']['target']}}">
          {{$section_data['lien']['title']}}
        </a>
      </div>
    @endif
  </div>
</section>
