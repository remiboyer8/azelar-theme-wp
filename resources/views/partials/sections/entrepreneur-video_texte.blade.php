

  <section class="entrepreneur entrepreneur__vimeo">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-5 gs_reveal gs_reveal_fromLeft">
          <div class="poster" data-toggle="modal" data-target="#modal-video" data-vimeo="{{$data_section->id_vimeo}}">
            <img
              src="{{wp_get_attachment_image_url( $data_section->vignette , 'm'  )}}"
              srcset="{{wp_get_attachment_image_srcset( $data_section->vignette )}}"
              sizes="(width:100vw) 700px"
              loading="lazy"
              alt="{{get_post_meta($data_section->vignette, '_wp_attachment_image_alt', TRUE)}}"
              class="img-fluid">
              <div class="play">@include('svg.play')</div>
          </div>
        </div>
        <div class="right col-md-7 gs_reveal gs_reveal_fromRight">
          <div class="title">{{$data_section->titre}}</div>
          <div class="text">
            {!! $data_section->texte !!}
          </div>
        </div>
      </div>
    </div>
  </section>
