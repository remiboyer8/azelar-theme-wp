<section class="section section__numbers">
  <div class="container">
    <div class="section-header gs_reveal">
      @if(strpos('[azelar]', $section_data['titre']) !== false)
        <img src="@asset('images/sprite/azelar.svg')" alt="azelar" class="d-md-none">
      @endif
      <div class="section-title">
        @if(isset($section_data['titre']) && !empty($section_data['titre']))
          <span>{!!$section_data['titre']!!}</span>
        @endif
      </div>
    </div>
    <div class="section-body ">
      @if(isset($section_data['nombre_1']) && !empty($section_data['nombre_1']))
      <div class="section-bloc  gs_reveal gs_reveal_fromLeft">
        <span class="gs_counter counter" data-count="{{$section_data['nombre_1']}}">{{$section_data['nombre_1']}}</span>
        <span class="label">{{$section_data['nombre_1_texte']}}</span>
      </div>
      @endif
      @if(isset($section_data['nombre_2']) && !empty($section_data['nombre_2']))
      <div class="section-bloc  gs_reveal gs_reveal_fromRight">
        <span class="gs_counter counter" data-count="{{$section_data['nombre_2']}}">{{$section_data['nombre_2']}}</span>
        <span class="label">{{$section_data['nombre_2_texte']}}</span>
      </div>
      @endif
    </div>
  </div>
</section>
<?php //dd($section_data); ?>
