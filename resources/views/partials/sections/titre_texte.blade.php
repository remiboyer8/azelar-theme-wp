
  <section class="entrepreneur entrepreneur__title_text">
    <div class="container ">
      <div class="row content">
        <div class="left col-md-8">
          <h3 class="title">
            {{$data_section->titre}}
          </h3>
          <div>
            {!! $data_section->texte !!}
          </div>
        </div>
        <div class="right col-md-7 ">

        </div>
      </div>
    </div>
  </section>
