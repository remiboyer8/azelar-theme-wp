<section class="entrepreneur entrepreneur__bloc_text">
  <div class="container ">
    <div class="row content">
      <div class="left col gs_reveal gs_reveal_fromLeft">
        <div class="bloc">
          {!! $data_section->texte_1 !!}
        </div>
      </div>
      <div class="right col gs_reveal gs_reveal_fromRight">
        <h3 class="title">{{ $data_section->titre_2 }}</h3>
        <div class="text">
          {!! $data_section->texte_2 !!}
        </div>
      </div>
    </div>
    <div class="dots"></div>
  </div>
</section>
