@if(isset($data) && is_array($data) && sizeof($data) > 0)
<section class="section section__carrousel gs_reveal {{isset($class) ? $class : ''}}">
  @if(isset($title))
  <div class="container px-xxl-0 maxw-1000">
    <div class="section-header gs_reveal {{(isset($description) && !empty($description)) ? 'mb-2' : ''}}">
      <h2 class="section-title">{!!$title!!}</span>
      </h2>
    </div>
  </div>
    {{-- @include('partials.sections.title', ['title'=>$title]) --}}
  @endif
  @if(isset($description) && !empty($description))
    <p class="mb-4">{!!$description!!}</p>
  @endif

  <div class="section-body">
    <div class="container swiper-carrousel-container">
      <div class="row">
        <div class="col-12 col-md-10 offset-md-1">
          <div class="swiper swiper-carrousel" data-controls="#swiper-controls-{{$id}}">
            <div class="swiper-wrapper">
              @foreach($data as $el)
              @if(isset($el['logo']) && $el['logo'] > 0)
              <div class="swiper-slide">
                <a href="{{$el['lien']}}" target="_blank">
                  <img src="{{wp_get_attachment_image_url( $el['logo'] , 'm'  )}}" class=""
                    data-srcset="{{wp_get_attachment_image_srcset( $el['logo'] )}}"
                    sizes="{{wp_get_attachment_image_sizes($el['logo'])}}"
                    alt="{{get_post_meta($el['logo'], '_wp_attachment_image_alt', TRUE)}}">

                </a>
              </div>
              @endif
              @endforeach

            </div>
          </div>
        </div>
      </div>

      {{-- @if(isset($el['logo']) && $el['logo'] > 0) --}}
      <div id="swiper-controls-{{$id}}">
        <div class="swiper-button-prev">
          @include('svg.arrow-left')
        </div>
        <div class="swiper-button-next">
          @include('svg.arrow-right')
        </div>
      </div>
      {{-- @endif --}}
    </div>
  </div>
</section>

@endif
