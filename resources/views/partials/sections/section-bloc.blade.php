<?php //dd($section_data); ?>
<section class="section section__bloc {{(isset($section_data['format']) && $section_data['format'] == 'left') ? 'section__bloc__left' : ''}}">
  <div class="container">
    <div class="bloc gs_reveal">
      @if(isset($section_data['illustration_1']) && !empty($section_data['illustration_1']))
        <img src="@asset('images/datas/'.$section_data['illustration_1'].'.svg')" class="illus1" alt="{{$section_data['illustration_1']}}">
      @endif
      @if(isset($section_data['illustration_2']) && !empty($section_data['illustration_2']))
        <img src="@asset('images/datas/'.$section_data['illustration_2'].'.svg')" class="illus2" alt="{{$section_data['illustration_2']}}">
      @endif
      <div class="content">
        <div class="section-header">
          @if(isset($section_data['titre']) && !empty($section_data['titre']))
          <{{(isset($is_h1) && $is_h1) ? 'h1' : 'h2'}} class="section-title section-title__{{$section_data['couleur_titre']}}" >
            {!!$section_data['titre']!!}
          </{{(isset($is_h1) && $is_h1) ? 'h1' : 'h2'}}>
          @endif
          @if(isset($section_data['logo']) && !empty($section_data['logo']))
          <div class="section-logo">
            <img src="@asset('images/datas/'.$section_data['logo'].'.svg')" alt="{{str_replace('-', ' ', $section_data['logo'])}}">
          </div>
          @endif
        </div>
        @if(isset($section_data['separateur']) && $section_data['separateur'])
        <hr>
        @endif

        <div class="section-body">
          @if(isset($section_data['texte']) && !empty($section_data['texte']))
            {!! $section_data['texte'] !!}
          @endif
        </div>
        @if(isset($section_data['lien']['title']) && !empty($section_data['lien']['title']))
        <a href="{{$section_data['lien']['url']}}" class="btn btn-annuaire  {{isset($section_data['lien_class']) ? str_replace('_', ' ', $section_data['lien_class']) : ''}}" target="{{$section_data['lien']['target']}}">
          @if(isset($section_data['lien_picto']) && !empty($section_data['lien_picto']))
            @include('svg.btn_'.$section_data['lien_picto'])
          @endif
          <span>{!!$section_data['lien']['title']!!}</span>
        </a>
        @endif

        @if(isset($section_data['texte_footer']) && !empty($section_data['texte_footer']))
          <div class="section-footer">
            {!! $section_data['texte_footer'] !!}
          </div>
        @endif
      </div>
    </div>
  </div>
</section>

@if(isset($section_data['dots']) && $section_data['dots'])
@include('partials.sections.dots', ['class'=>'dots__center section__bloc-dots '])
@endif

@if(isset($section_data['outer_texte']) && !empty($section_data['outer_texte']))
@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])
<div class="container">
  <div class="section-footer">
  {!!$section_data['outer_texte']!!}
  </div>
</div>
@endif
