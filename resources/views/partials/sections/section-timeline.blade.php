<section class="section section__timeline">
  <div class="container">
    @if(isset($section_data['titre']) && !empty($section_data['titre']))
    <div class="section-header gs_reveal">
      <h2 class="section-title">
        {{$section_data['titre']}}
      </h2>
    </div>
    @endif
    <div class="row section-body gs_reveal">

      <div class="col-12 col-md-6 section-left">
        <ul class="steps">
          @if(isset($section_data['etapes']) && is_array($section_data['etapes']) && sizeof($section_data['etapes']) > 0)
            @foreach($section_data['etapes'] as $k=>$etape)
            <li class="{{$etape['couleur']}}">
              @include('partials.cards.card-flap',[
                'icon'=>'plus',
                'class'=>$etape['couleur'].' card__flap__vertical',
                'recto'=>$etape['recto'],
                'verso'=>$etape['verso']
                ])
            </li>
            @endforeach
          @endif

        </ul>
      </div>
      <div class="col-12 col-md-6 section-right">
        <div class="bloc gs_reveal gs_reveal_fromRight">
          @if(isset($section_data['titre_droite']) && !empty($section_data['titre_droite']))
          <div class="section-title">{!!$section_data['titre_droite']!!}</div>
          @endif
          @if(isset($section_data['texte_droite']) && !empty($section_data['texte_droite']))
          <div class="text">{!!$section_data['texte_droite']!!}</div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>




