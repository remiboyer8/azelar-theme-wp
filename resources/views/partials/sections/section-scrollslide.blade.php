<section class="section section__scroll-slide">
  @isset($section_data['titre'])
  <div class="section-header gs_reveal">
    <{{(isset($is_h1) && $is_h1) ? 'h1' : 'h2'}} class="section-title ">{{$section_data['titre']}}</{{(isset($is_h1) && $is_h1) ? 'h1' : 'h2'}}>
  </div>
  @endisset
  @if(isset($section_data['textes']) && is_array($section_data['textes']) && sizeof($section_data['textes']) > 0)
  <div class="section-body ">
    @foreach($section_data['textes'] as $texte)
    <div class="scroll-slide-panel gs_reveal">
      <div class="wrapper">{!!$texte['texte']!!}</div>
    </div>
    @endforeach
  </div>
  @endif
  @isset($section_data['lien']['title'])
  <div class="section-footer  gs_reveal">
    <a href="{{$section_data['lien']['url']}}" class="btn btn-primary" target="{{$section_data['lien']['target']}}">{{$section_data['lien']['title']}}</a>
  </div>
  @endisset
</section>

{{-- <section class="section section__scroll-slide-mobile">

</section> --}}
