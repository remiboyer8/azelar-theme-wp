<div class="main-header d-lg-none">
  <a href="#" class="azelar-header">
    <img src="@asset('images/sprite/azelar.svg')" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav"
    aria-expanded="false" aria-label="Toggle navigation">
    <div class="wrapper">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </button>
</div>
