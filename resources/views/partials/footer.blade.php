
@if (is_home() || (is_single() && 'post' == get_post_type()) || (isset($page_fields['prefooter_active']) && $page_fields['prefooter_active']))

  @include('partials.sections.spacer', ['desktop' => 30, 'mobile' => 15])

  <section class="section section__bloc section__bloc__left">
    <div class="container">
      <div class="offset-md-2 col-md-8">
        <div class="bloc gs_reveal"
          style="opacity: 1; visibility: inherit; transform: translate(0px);padding: 20px 90px;">
          <div class="content">
            <div class="section-header">
              <h2 class="section-title section-title__rouge">

                @if (isset($global_options['prefooter_titre']))
                  {!! $global_options['prefooter_titre'] !!}
                @endif
              </h2>
            </div>

            <div class="section-body">
              @if (isset($global_options['prefooter_content']))
                {!! $global_options['prefooter_content'] !!}
              @endif
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>

@endif

<footer class="main-footer content-info">
  <div class="container">
    {{-- @php dynamic_sidebar('sidebar-footer') @endphp --}}
    <img class="logo" src="@asset('images/sprite/azelar-white.svg')" alt="azelar">
    <div class="row footer-header">
      <div class="col-12  col-lg-3">
        &nbsp;
      </div>
      <div class="col-12  col-lg-3">
        {{-- <div class="title">
          @if (isset($global_options['footer_titre_joindre'])){{$global_options['footer_titre_joindre']}}@endif</div> --}}
      </div>
      <div class="col-12 col-lg-3 ">
        {{-- <div class="title">
          @if (isset($global_options['footer_titre_suivre'])){{$global_options['footer_titre_suivre']}}@endif</div> --}}
      </div>
      <div class="col-12 col-lg-3">
        &nbsp;
      </div>
    </div>
    <div class="row footer-body">
      <div class="col-12 col-lg-3">
        @if (isset($global_options['contact_adresse']))
          {!! $global_options['contact_adresse'] !!}
        @endif
      </div>
      <div class="col-12 col-lg-3 contact">
        <div class="title">
          @if (isset($global_options['footer_titre_joindre']))
            {{ $global_options['footer_titre_joindre'] }}
          @endif
        </div>
        @if (isset($global_options['contact_telephone']))
          <a href="tel:{{ str_replace(' ', '', $global_options['contact_telephone']) }}"
            class="point-link">{{ $global_options['contact_telephone'] }}</a>
        @endif
        @if (isset($global_options['contact_email']))
          <a href="mailto:{{ $global_options['contact_email'] }}">{{ $global_options['contact_email'] }}</a>
        @endif
      </div>
      <div class="col-12 col-lg-3 socials ">
        <div class="title ">
          @if (isset($global_options['footer_titre_suivre']))
            {{ $global_options['footer_titre_suivre'] }}
          @endif
        </div>
        @if (isset($global_options['facebook']) && !empty($global_options['facebook']))
          <a href="{{ $global_options['facebook'] }}" class="fill__white"
            target="_blank">@include('svg.facebook')</a>
        @endif
        @if (isset($global_options['linkedin']) && !empty($global_options['linkedin']))
          <a href="{{ $global_options['linkedin'] }}" class="fill__white"
            target="_blank">@include('svg.linkedin')</a>
        @endif
        @if (isset($global_options['instagram']) && !empty($global_options['instagram']))
          <a href="{{ $global_options['instagram'] }}" class="fill__white"
            target="_blank">@include('svg.instagram')</a>
        @endif
      </div>
      <div class="col-12 col-lg-3 corpo  ">

        @if (isset($navfooter) && is_array($navfooter))
          @foreach ($navfooter as $k => $nav_item)
            <a href="{{ $nav_item->url }}"
              class="{{ $k + 1 > sizeof($navfooter) ? 'point-link' : '' }}">{{ $nav_item->title }}</a>
          @endforeach
        @endif
        {{-- <a href="#" class="point-link">Mentions légales</a>
        <a href="#">Politique de confidentialité</a> --}}
        <div>
          @2021&nbsp;-&nbsp;@if (isset($global_options['footer_copyright']))
            {{ $global_options['footer_copyright'] }}
          @endif
        </div>
      </div>
    </div>
  </div>
</footer>
