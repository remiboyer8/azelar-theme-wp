{{-- <div class="page-header">
  <h1>{!! App::title() !!}</h1>
</div> --}}
@if(isset($title) && !empty($title))
<div class="page-header">
  <div class="container">
    <h1 class="page-title">{!!$title!!}</h1>
    @if(isset($subtitle) && !empty($subtitle))
    <h2  class="page-subtitle">
      {!!$subtitle!!}
    </h2>
    @endif
  </div>
</div>
@endif
