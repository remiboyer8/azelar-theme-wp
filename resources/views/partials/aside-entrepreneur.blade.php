<button class="navbar-toggler aside-navbar-toggler js-toggle-element-class" type="button" data-toggle="collapse"
  data-target="#aside-entrepreneur" data-class="js-aside-open" data-element="body" aria-controls="aside-entrepreneur"
  aria-expanded="true" aria-label="Toggle aside">
  <div class="wrapper">
    <span></span>
    <span></span>
    <span></span>
  </div>
</button>

<nav id="aside-entrepreneur" class="collapse aside-collapse show">
  <div class="aside-header">
    <img src="{{wp_get_attachment_image_url( $data->vignette , 'm'  )}}" class="aside-img"
      srcset="{{wp_get_attachment_image_srcset( $data->vignette )}}" sizes="(width:100vw) 350px"
      alt="{{$data->prenom}} {{$data->nom}}">
  </div>
  <div class="aside-body">
    <div class="motif">
      @foreach ($poles_metiers as $k=>$pole_metier)
      @foreach ($data->poles_metiers as $pole_metier_id)
      @if($pole_metier_id == $pole_metier['id'] && isset($pole_metier['image']) && $pole_metier['image'] > 0)
      <a href="{{$pole_metier['url']}}" class="d-inline">
        <img src="{{wp_get_attachment_image_url( $pole_metier['image'] , 's'  )}}"
          alt="{{get_post_meta($pole_metier['image'], '_wp_attachment_image_alt', TRUE)}}">
      </a>
      @endif
      @endforeach
      @endforeach
      {{-- <img src="@asset('images/datas/motif1.png')" alt="">
      <img src="@asset('images/datas/motif2.png')" alt="">
      <img src="@asset('images/datas/motif3.png')" alt=""> --}}
    </div>
    <h3 class="title">{{$data->prenom}} {{$data->nom}}</h3>
    @if(!empty($data->nom_commercial))
    <h4 class="subtitle">{{$data->nom_commercial}}</h4>
    @endif
    @if(!empty($data->site_internet))
    <a href="{{$data->site_internet}}" target="_blank">{{preg_replace( "#^[^:/.]*[:/]+#i", "",
      $data->site_internet)}}</a>
    @endif
    <div class="sep"></div>
    @if(!empty($data->telephone))
    <a href="tel:{{str_replace(' ', '', $data->telephone)}}">{{$data->telephone}}</a>
    @endif
    @if(!empty($data->telephone))
    <a href="mailto:{{$data->email}}">{{$data->email}}</a>
    @endif
    @if(!empty($data->lieu_activite))
    <div class="location">{{$data->lieu_activite}}</div>
    @endif

    <div class="socials">
      @if(is_array($data->liens))
      @foreach($data->liens as $type=>$url)
      @if(!empty($url))
      <a href="{{$url}}" target="_blank">@include('svg.'.$type)</a>
      @endif
      @endforeach
      @endif
      {{-- @foreach($data->liens as $reseau=>$lien)
      @if(!empty($lien))
      <a href="{{$lien}}" target="_blank">$reseau</a>
      @endif
      @endforeach --}}
      {{-- <a href="#">@include('svg.facebook_blue')</a>
      <a href="#">@include('svg.linkedin_blue')</a>
      <a href="#">@include('svg.insta')</a> --}}
    </div>

    <div class="tags">
      @if(!empty($data->hashtag_1))
      <h5 class="tag tag__red">{{$data->hashtag_1}}</h5>
      @endif
      @if(!empty($data->hashtag_2))
      <h5 class="tag tag__yellow">{{$data->hashtag_2}}</h5>
      @endif
      @if(!empty($data->hashtag_3))
      <h5 class="tag tag__blue">{{$data->hashtag_3}}</h5>
      @endif
    </div>
    </div.>
</nav>
