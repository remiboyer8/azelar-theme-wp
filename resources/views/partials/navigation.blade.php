<nav id="mainNav" class="js-side-navigation navbar navbar-expand-lg collapse navbar-collapse">
  <div class="wrapper">
    <button class="close" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav"
      aria-expanded="false" aria-label="Toggle navigation">
      @include('svg.croix')
    </button>
    <a href="{{ home_url('/') }}" class="azelar">
      <img src="@asset('images/sprite/azelar-v.svg')" alt="" class="d-none d-lg-block">
      <img src="@asset('images/sprite/azelar.svg')" alt="" class="d-lg-none">
    </a>

    <ul class="navbar-nav">
      @if(isset($navmain) && is_array($navmain))
        @foreach($navmain as $nav_item)
        @php $current_id = get_the_ID(); $is_active = ($current_id == $nav_item->object_id); @endphp
        <li class="nav-item {{($is_active) ? 'active' : ''}}">
          <a href="{{$nav_item->url}}" class="nav-link">
            {{$nav_item->title}}@if($is_active)<span class="sr-only">(current)</span>@endif
          </a>
        </li>
        @endforeach
      @endif
    </ul>
    <div class="socials">
      @if(isset($global_options['facebook']) && !empty($global_options['facebook']))
      <a href="{{$global_options['facebook']}}" class="fill__red-med" target="_blank">@include('svg.facebook')</a>
      @endif
      @if(isset($global_options['linkedin']) && !empty($global_options['linkedin']))
      <a href="{{$global_options['linkedin']}}" class="fill__blue" target="_blank">@include('svg.linkedin')</a>
      @endif
      @if(isset($global_options['instagram']) && !empty($global_options['instagram']))
      <a href="{{$global_options['instagram']}}" class="fill__yellow" target="_blank">@include('svg.instagram')</a>
      @endif
    </div>
  </div>
</nav>
