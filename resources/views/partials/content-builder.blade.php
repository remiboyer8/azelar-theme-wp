@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@include('partials.sections.section-bloc',
[
'class'=>'',
'illus1'=>'images/datas/carre.svg',
'illus2'=>'images/datas/illus5.svg',
'logo'=>'',
'title'=>'Vous êtes un professionnel du secteur culturel,<br>un artiste ou un créatif&nbsp;? ',
'hr'=>true,
'body'=>'Pour stabiliser, pérenniser et enrichir vos activités, nous proposons à travers Azelar
une&nbsp;dynamique&nbsp;coopérative, engagée et sécurisante.',
'button_class'=>'btn-primary',
'button_icon'=>'',
'button_label'=>'nous rejoindre',
'button_link'=>'#',
'footer'=>'',
'dots'=>true,
])


@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.section-flaps')

@include('partials.sections.spacer', ['desktop'=>90, 'mobile'=>30 ])

@php
$s = '<img src="'.get_stylesheet_directory_uri().'/assets/images/sprite/azelar.svg" alt="azelar"> s\'adresse à 6
grands pôles métiers...'
@endphp

@include('partials.sections.title', ['title' =>$s])

@include('partials.sections.section-metiers', ['footer'=>false])

@include('partials.sections.spacer', ['desktop'=>100, 'mobile'=>40 ])

@include('partials.sections.section-tags')

@include('partials.sections.spacer', ['desktop'=>50, 'mobile'=>30 ])

@include('partials.sections.section-timeline')


@include('partials.sections.section-bloc',
[
'class'=>'section__bloc__left',
'illus1'=>'images/datas/losange.svg',
'illus2'=>'',
'logo'=>'',
'title'=>'Une gouvernance partagée et démocratique pour prendre part aux grandes orientations de la structure.',
'hr'=>false,
'body'=>'La coopérative propose à chacun de s’investir dans la mesure de ses capacités : entraide, échanges, partage
d’expérience, mutualisation de compétences ou de moyens…
Chacun peut prendre part aux décisions stratégiques du projet en devenant associé, pour que le fonctionnement d’Azelar
évolue au plus proche des préoccupations de ses coopérateurs.',
'button_class'=>'btn-primary',
'button_icon'=>'',
'button_label'=>'Les chroniques d\'azelar',
'button_link'=>'#',
'footer'=>'',
'dots'=>false,
])


@include('partials.sections.section-testimony')

@include('partials.sections.button', ['label'=>'découvrir les cooperateurs', 'link'=>'#',
'icon'=>'<svg width="44" height="31" viewBox="0 0 44 31" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0)">
    <path
      d="M18.3 5.1L0 0V25.3L17.9 30.4H43.2V5.1H18.3ZM17.9 28.7L1.7 23.9V1.9L17.9 6.7V28.7ZM41.5 28.7H19.5V6.7H41.5V28.7Z"
      fill="white" />
    <path d="M38.6 10.3H32.1V12H38.6V10.3Z" fill="white" />
    <path d="M38.6 14.7H32.1V16.4H38.6V14.7Z" fill="white" />
    <path d="M38.6 19H32.1V20.7H38.6V19Z" fill="white" />
    <path d="M38.6 23.4H32.1V25.1H38.6V23.4Z" fill="white" />
    <path d="M28.9 10.4H22.4V12.1H28.9V10.4Z" fill="white" />
    <path d="M28.9 14.8H22.4V16.5H28.9V14.8Z" fill="white" />
    <path d="M28.9 19.2H22.4V20.9H28.9V19.2Z" fill="white" />
    <path d="M28.9 23.5H22.4V25.2H28.9V23.5Z" fill="white" />
  </g>
  <defs>
    <clipPath id="clip0">
      <rect width="43.2" height="30.4" fill="white" />
    </clipPath>
  </defs>
</svg>
','class'=>'btn-annuaire btn-primary' ])

@include('partials.sections.spacer', ['desktop'=>50, 'mobile'=>30 ])

@include('partials.sections.section-joinus')

PAGE ecosysteme

@include('partials.sections.section-bloc',
[
'class'=>'',
'illus1'=>'images/datas/illus2.svg',
'illus2'=>'images/datas/losange.svg',
'logo'=>'images/datas/graines-de-sol.svg',
'title'=>'Azelar est portée par la<br>Coopérative d’Activité et d’Emploi',

'hr'=>true,
'body'=>' Initié par les associés de Graines de SOL pour mieux répondre aux besoins du territoire, le pôle art et
culture bénéficie des nombreuses années d’expérience et du fonctionnement mutualisé de la coopérative multi-activités.',
'button_class'=>'btn-annuaire btn-primary',
'button_icon'=>'<svg width="44" height="31" viewBox="0 0 44 31" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0)">
    <path
      d="M18.3 5.1L0 0V25.3L17.9 30.4H43.2V5.1H18.3ZM17.9 28.7L1.7 23.9V1.9L17.9 6.7V28.7ZM41.5 28.7H19.5V6.7H41.5V28.7Z"
      fill="white" />
    <path d="M38.6 10.3H32.1V12H38.6V10.3Z" fill="white" />
    <path d="M38.6 14.7H32.1V16.4H38.6V14.7Z" fill="white" />
    <path d="M38.6 19H32.1V20.7H38.6V19Z" fill="white" />
    <path d="M38.6 23.4H32.1V25.1H38.6V23.4Z" fill="white" />
    <path d="M28.9 10.4H22.4V12.1H28.9V10.4Z" fill="white" />
    <path d="M28.9 14.8H22.4V16.5H28.9V14.8Z" fill="white" />
    <path d="M28.9 19.2H22.4V20.9H28.9V19.2Z" fill="white" />
    <path d="M28.9 23.5H22.4V25.2H28.9V23.5Z" fill="white" />
  </g>
  <defs>
    <clipPath id="clip0">
      <rect width="43.2" height="30.4" fill="white" />
    </clipPath>
  </defs>
</svg>',
'button_label'=>'annuaire des COOPÉRATEURS',
'button_link'=>'#',
'footer'=>'Les coopérateurs sont secondés par une équipe d\'appui engagée, qui leur assure une meilleure disponibilité
pour leurs productions.',
'dots'=>true,
])


@include('partials.sections.section-team-azelar')

@include('partials.sections.section-team-gds')

@include('partials.sections.section-bloc',
[
'class'=>'section__bloc__left',
'illus1'=>'/dist/images/datas/illus2.svg',
'illus2'=>'',
'logo'=>'',
'title'=>'Azelar est un lieu de travail collectif à la jonction de l’économie sociale et solidaire et du secteur
artistique et culturel. ',
'hr'=>false,
'body'=>'<div style="text-align:left"> <em>
    La coopérative se veut complémentaire des initiatives locales et nationales préexistantes.
  </em>
  <br>
  <strong>
    Nous favorisons et tissons des collaborations avec :
  </strong>
  <ul>
    <li>Les acteurs culturels et les réseaux sectoriels</li>
    <li>Les structures d’accompagnement expertes en leurs domaines</li>
    <li>Des (tiers-)lieux favorisant l’ouverture et l’échange</li>
    <li>Les acteurs de la création d’entreprise</li>
    <li>Les acteurs de l’ESS et du monde coopératif</li>
    <li>Les collectivités</li>
  </ul>
</div>',
'button_class'=>'',
'button_icon'=>'',
'button_label'=>'',
'button_link'=>'',
'footer'=>'',
'dots'=>false,
])



@include('partials.sections.section-carrousel', ['class'=>'bg-white', 'title'=>'Partenaires financeurs',
'id'=>'partners'])

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.section-carrousel', ['class'=>'bg-white',
'title'=>'Collectivités associées', 'id'=>'collect'])

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.section-carrousel', ['class'=>'bg-white', 'title'=>'Réseaux', 'id'=>'reseau'])


Page Article List

<div class="container">

  <ul class="ul__categories">
    <li><a href="#">Tout afficher</a></li>
    <li class="active"><a href="#">Focus sur la coopérative</a></li>
    <li><a href="#">Récits de coopérateurs</a></li>
    <li><a href="#">Projets collectifs</a></li>
    <li><a href="#">Ressources inspirantes </a></li>
    <li><a href="#">On parle de nous</a></li>
  </ul>

  <div class="row articles">
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
    <div class="col-12 col-md-4 article-item">
      @include('partials.cards.card-blog')
    </div>
  </div>

</div>

Page Article


<article class="article">
  <div class="container">
    <img src="@asset('images/datas/img.jpg')" class="img-fluid mx-auto" alt="">

    <section class="section section__text-left">
      <div class="row">
        <div class="col-12 col-md-8">
          <div class="section-title">Advenit post multos Scudilo advenit suctilo.</div>
          <div class="section-text">
            <strong>Advenit post multos Scudilo Scutariorum</strong>
            tribunus velamento subagrestis ingenii persuasionis opifex callidus. qui eum adulabili sermone seriis
            admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod flagrantibus votis eum
            videre frater cuperet patruelis adsimulato saepius lus omnium proficisci pellexit vultu adsimulato saepius
            replicando quod.
          </div>
        </div>
      </div>
    </section>

    @include('partials.sections.dots', ['class'=>'dots__left'])

    {{-- @include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ]) --}}

    <section class="section section__text-left section__wysiwyg">
      <p>Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex callidus.
        qui
        eum adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando olus
        omnium proficisci pellexit vult s opifex callidus. qui eum adulabili sermone seriis admixto solus omnium
        proficisci pell. Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis
        opifex.
        Soleo saepe ante oculos ponere, idque libenter crebris usurpare sermonibus, omnis nostrorum imperatorum, omnis
        exterarum gentium potentissimorumque populorum, omnis clarissimorum regum res gestas, cum tuis nec contentionum
        magnitudine nec numero proeliorum nec varietate regionum nec celeritate conficiendi nec dissimilitudine bellorum
        posse conferri; nec vero disiunctissimas terras citius passibus cuiusquam potuisse peragrari, quam tuis non
        dicam
        cursibus, sed victoriis lustratae sunt.<br><br>

        Nihil morati post haec militares avidi saepe turbarum adorti sunt Montium primum, qui divertebat in proximo,
        levi
        corpore senem atque morbosum, et hirsutis resticulis cruribus eius innexis divaricaturn sine spiramento ullo ad
        usque praetorium traxere praefecti.</p>


      <p>Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis opifex callidus.
        qui
        eum adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando olus
        omnium proficisci pellexit vult s opifex callidus. qui eum adulabili sermone seriis admixto solus omnium
        proficisci pell. Advenit post multos Scudilo Scutariorum tribunus velamento subagrestis ingenii persuasionis
        opifex.
        Soleo saepe ante oculos ponere, idque libenter crebris usurpare sermonibus, omnis nostrorum imperatorum, omnis
        exterarum gentium potentissimorumque populorum, omnis clarissimorum regum res gestas, cum tuis nec contentionum
        magnitudine nec numero proeliorum nec varietate regionum nec celeritate conficiendi nec dissimilitudine bellorum
        posse conferri; nec vero disiunctissimas terras citius passibus cuiusquam potuisse peragrari, quam tuis non
        dicam
        cursibus, sed victoriis lustratae sunt.<br><br>

        Nihil morati post haec militares avidi saepe turbarum adorti sunt Montium primum, qui divertebat in proximo,
        levi
        corpore senem atque morbosum, et hirsutis resticulis cruribus eius innexis divaricaturn sine spiramento ullo ad
        usque praetorium traxere praefecti.</p>

    </section>

  </div>
</article>
PAGE cooperative



@include('partials.sections.section-bloc',
[
'class'=>'',
'illus1'=>'images/datas/losange.svg',
'illus2'=>'images/datas/pyramid.svg',
'logo'=>'',
'title'=>'Azelar est née d’une volonté commune : proposer une organisation agile facilitant l’exercice du travail
artistique, culturel et créatif.',
'hr'=>false,
'body'=>'',
'button_class'=>'btn-annuaire btn-primary',
'button_icon'=>'<svg width="44" height="31" viewBox="0 0 44 31" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0)">
    <path
      d="M18.3 5.1L0 0V25.3L17.9 30.4H43.2V5.1H18.3ZM17.9 28.7L1.7 23.9V1.9L17.9 6.7V28.7ZM41.5 28.7H19.5V6.7H41.5V28.7Z"
      fill="white" />
    <path d="M38.6 10.3H32.1V12H38.6V10.3Z" fill="white" />
    <path d="M38.6 14.7H32.1V16.4H38.6V14.7Z" fill="white" />
    <path d="M38.6 19H32.1V20.7H38.6V19Z" fill="white" />
    <path d="M38.6 23.4H32.1V25.1H38.6V23.4Z" fill="white" />
    <path d="M28.9 10.4H22.4V12.1H28.9V10.4Z" fill="white" />
    <path d="M28.9 14.8H22.4V16.5H28.9V14.8Z" fill="white" />
    <path d="M28.9 19.2H22.4V20.9H28.9V19.2Z" fill="white" />
    <path d="M28.9 23.5H22.4V25.2H28.9V23.5Z" fill="white" />
  </g>
  <defs>
    <clipPath id="clip0">
      <rect width="43.2" height="30.4" fill="white" />
    </clipPath>
  </defs>
</svg>',
'button_label'=>'annuaire des COOPÉRATEURS',
'button_link'=>'#',
'footer'=>'',
'dots'=>true,
])



@include('partials.sections.section-twoblocs')

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])




@include('partials.sections.section-bloc',
[
'class'=>'section__bloc__left',
'illus1'=>'/dist/images/datas/losange.svg',
'illus2'=>'',
'logo'=>'',
'title'=>' Nous tenons à l’autonomie des artistes et créatifs pour garantir leur liberté de création.',
'hr'=>false,
'body'=>'<div style="text-align:left"> <strong>
    Le pilotage d’une activité individuelle au sein d’une entreprise partagée apporte des réponses aux enjeux de
    la professionnalisation culturelle :
  </strong>
  <ul>
    <li>Stabilité économique</li>
    <li>Reconnaissance du travail artistique</li>
    <li>Allègement des démarches administratives</li>
    <li>Simplification des statuts</li>
    <li>Inscription dans des collectifs solides et durables.</li>
  </ul>
</div>',
'button_class'=>'',
'button_icon'=>'',
'button_label'=>'',
'button_link'=>'',
'footer'=>'',
'dots'=>false,
])



@include('partials.sections.button', ['label'=>'les chroniques d\'azelar', 'link'=>'#',
'icon'=>'','class'=>'btn-secondary' ])

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@include('partials.sections.section-text')

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.button', ['label'=>'Nous rejoindre', 'link'=>'#',
'icon'=>'','class'=>'btn-secondary' ])

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@include('partials.sections.section-testimony')

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])

@include('partials.sections.button', ['label'=>'Nous contacter', 'link'=>'#',
'icon'=>'','class'=>'btn-secondary' ])

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])
