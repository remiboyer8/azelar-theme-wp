{{--
  Template Name: Ecosystème
--}}

@extends('layouts.app')

@section('content')

@include('partials.sections.section-bloc',
[
  'is_h1' => true,
  'section_data' => $page_fields['intro'],
])


@include('partials.sections.section-team-azelar', ['section_data' => $page_fields['team_azelar']])

@include('partials.sections.section-team-gds', ['section_data' => $page_fields['team_gds']])

@include('partials.sections.section-bloc',
[
  'section_data' => $page_fields['lieu'],
])

@include('partials.sections.spacer', ['desktop'=>90, 'mobile'=>30 ])

@if(isset($page_fields['partenaires']) && is_array($page_fields['partenaires']))
  @foreach($page_fields['partenaires'] as $k=>$categorie_partenaires)
    @if(isset($categorie_partenaires->name))
      @include('partials.sections.section-carrousel', [
        'class'=>'bg-white',
        'data' => get_partenaires($categorie_partenaires->term_id),
        'title'=>$categorie_partenaires->name,
        'description'=>$categorie_partenaires->description,
        'id'=>$categorie_partenaires->slug
        ])
    @endif
    @if(($k+1) < sizeof($page_fields['partenaires']))
      @include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])
    @endif
  @endforeach
@endif

@include('partials.sections.spacer', ['desktop'=>60, 'mobile'=>30 ])

@include('partials.sections.button', ['section_data' => $page_fields['contact']])

@include('partials.sections.spacer', ['desktop'=>30, 'mobile'=>30 ])


@endsection
