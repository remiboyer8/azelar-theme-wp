<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleEntrepreneur extends Controller
{
    public function data()
    {
        $output = get_fields();
        $output = $this->multi_dimensional_arr_to_obj($output);

        $output->cover_slider = isset($output->cover_slider) ? $this->obj_to_arr($output->cover_slider) : array();
        $output->sections = isset($output->sections) ? $this->obj_to_arr($output->sections) : array();
        $output->liens = isset($output->liens) ? $this->obj_to_arr($output->liens) : array();

        return $output;
    }
    private function multi_dimensional_arr_to_obj($arr)
    {
        return (is_array($arr) ? (object) array_map( array($this, 'multi_dimensional_arr_to_obj'), $arr) : $arr);
    }
    private function obj_to_arr($data)
    {
        return (isset($data) && is_object($data)) ? (array) $data : array();
    }

}
