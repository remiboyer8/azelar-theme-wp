<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Index extends Controller
{
    public function posts()
    {
        $get_queried_object = get_queried_object();
        $category_id = (isset($get_queried_object->term_id)) ? $get_queried_object->term_id : false;

        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
        $posts = get_posts( array(
            'post_type' => 'post',
            'numberposts' => get_option( 'posts_per_page' ),
			'paged' => $paged,
            'category' => $category_id
        ));
        foreach($posts as $k=>$v) {
            $tmp = get_fields($v->ID);
            $tmp['id'] = $v->ID;
            $tmp['url'] = get_permalink($v->ID);
            $cats = get_the_category($v->ID);
            if(is_array($cats)) {
                foreach($cats as $kc=>$vc) {
                    $tmpc = array(
                        'id' => $vc->term_id,
                        'url' => get_term_link($vc->term_id, 'category'),
                        'title' => $vc->name,
                    );
                    $cats[$kc] = $tmpc;
                }
            }
            $tmp['categories'] = $cats;

            $posts[$k] = $tmp;
        }
        return $posts;
    }

    public function categories()
    {
        $output = get_categories();
        return $output;
    }

}
