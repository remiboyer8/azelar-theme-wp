<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{

    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function navmain()
    {
        $menuLocations = get_nav_menu_locations();
        $menuID = $menuLocations['primary_navigation'];
        return wp_get_nav_menu_items($menuID);
    }

    public function navfooter()
    {
        $menuLocations = get_nav_menu_locations();
        $menuID = $menuLocations['footer_navigation'];
        return wp_get_nav_menu_items($menuID);
    }

    public function globalOptions()
    {
        $output = get_fields( 'options');
        if(isset($output['poles_metiers']['poles_metiers']) && is_array($output['poles_metiers']['poles_metiers'])) {
            foreach($output['poles_metiers']['poles_metiers'] as $k=>$v) {
                $tmp = get_fields( 'poles_metiers_'.$v->term_id);
                $tmp['id'] = $v->term_id;
                $tmp['title'] = $v->name;
                $tmp['url'] = get_term_link($v->term_id, 'poles_metiers');
                $output['poles_metiers']['poles_metiers'][$k] = $tmp;
            }
        }
        return $output;
    }

    public function pageFields()
    {
        $output = get_fields();

        $output['sections']['sections'] = $this->obj_to_arr(isset($output['sections']['sections']) ? $this->multi_dimensional_arr_to_obj($output['sections']['sections']) : false);
        return $output;
    }
    private function multi_dimensional_arr_to_obj($arr)
    {
        return (is_array($arr) ? (object) array_map( array($this, 'multi_dimensional_arr_to_obj'), $arr) : $arr);
    }
    private function obj_to_arr($data)
    {
        return (isset($data) && is_object($data)) ? (array) $data : array();
    }

    public function firstActus()
    {
        $posts = get_posts( array(
            'post_type' => 'post',
            'numberposts' => 3
        ));
        foreach($posts as $k=>$v) {
            $tmp = get_fields($v->ID);
            $tmp['id'] = $v->ID;
            $tmp['url'] = get_permalink($v->ID);
            $cats = get_the_category($v->ID);
            if(is_array($cats)) {
                foreach($cats as $kc=>$vc) {
                    $tmpc = array(
                        'id' => $vc->term_id,
                        'url' => get_term_link($vc->term_id, 'category'),
                        'title' => $vc->name,
                    );
                    $cats[$kc] = $tmpc;
                }
            }
            $tmp['categories'] = $cats;

            $posts[$k] = $tmp;
        }
        return $posts;
    }

    public function metiers()
    {
        $entrepreneurs = $this->entrepreneurs();
        $metiers_valides = array();
        foreach($entrepreneurs as $entrepreneur) {
            if(isset($entrepreneur['metiers']) && is_array($entrepreneur['metiers'])) {
                $metiers_valides = array_merge($metiers_valides, $entrepreneur['metiers']);
            }
        }
        $metiers_valides = array_unique($metiers_valides);

        $metiers = get_terms( array(
            'taxonomy' => 'metiers',
            'hide_empty' => true,
        ));
        $output = array();
        foreach($metiers as $metier) {
            if(in_array($metier->term_id, $metiers_valides)) {
                $tmp = array(
                    'id' => $metier->term_id,
                    'nom' => $metier->name,
                    'alt' => get_field('search_name', 'metiers_'.$metier->term_id)
                );
                $output[] = $tmp;
            }
        }
        return $output;
    }

    public function polesMetiers()
    {
        $poles_metiers = get_terms( array(
            'taxonomy' => 'poles_metiers',
            'hide_empty' => true,
        ));
        $output = array();
        foreach($poles_metiers as $pole_metier) {
            $titre_sidebar = get_field('annuaire_titre_sidebar', 'poles_metiers_'.$pole_metier->term_id);
            $tmp = array(
                'id' => $pole_metier->term_id,
                'url' => get_term_link($pole_metier->term_id, 'poles_metiers'),
                'color' => get_field('color', 'poles_metiers_'.$pole_metier->term_id),
                'picto' => get_field('picto', 'poles_metiers_'.$pole_metier->term_id),
                'image' => get_field('image', 'poles_metiers_'.$pole_metier->term_id),
                'nom' => (!empty($titre_sidebar)) ? $titre_sidebar : $pole_metier->name
            );
            $output[] = $tmp;
        }
        return $output;
    }

    public function selectEntrepreneurs()
    {
        $args = array(
            'post_type' => 'entrepreneur',
            'numberposts' => -1,
            'meta_key'			=> 'nom',
            'orderby'			=> 'meta_value',
            'order'				=> 'ASC'
        );
        // $get_queried_object = get_queried_object();
        // if(isset($get_queried_object->term_id)) {
        //     $pole_metier_id = get_queried_object()->term_id;
        //     if(intval($pole_metier_id) > 0) {
        //         $args['tax_query'] = array(array(
        //             'taxonomy' => 'poles_metiers',
        //             'field'    => 'term_id',
        //             'terms'    => intval($pole_metier_id),
        //         ));
        //     }
        // }
        $entrepreneurs = get_posts( $args );
        $output = array();
        foreach($entrepreneurs as $entrepreneur) {
            $tmp = array(
                'id' => $entrepreneur->ID,
                'url' => get_permalink($entrepreneur->ID),
                'nom' => get_field('nom', $entrepreneur->ID),
                'prenom' => get_field('prenom', $entrepreneur->ID),
                'nom_commercial' => get_field('nom_commercial', $entrepreneur->ID)
            );
            $output[] = $tmp;
        }
        return $output;
    }


    public function entrepreneurs()
    {
        $args = array(
            'post_type' => 'entrepreneur',
            'numberposts' => -1, //$limit,
        );

        $get_queried_object = get_queried_object();
        if(isset($get_queried_object->term_id)) {
            $pole_metier_id = get_queried_object()->term_id;
            if(intval($pole_metier_id) > 0) {
                $args['tax_query'] = array(array(
                    'taxonomy' => 'poles_metiers',
                    'field'    => 'term_id',
                    'terms'    => intval($pole_metier_id),
                ));
            }
        }

        $entrepreneurs = get_posts($args);
        // var_dump(sizeof($entrepreneurs), $args);
        $output = array();
        foreach($entrepreneurs as $entrepreneur) {
            $tmp = get_fields($entrepreneur->ID);
            $tmp['id'] = $entrepreneur->ID;
            $tmp['url'] = get_permalink($entrepreneur->ID);
            $output[] = $tmp;
        }
        // if(isset($_SESSION['entrepreneurs_random']))
        // var_dump($_SESSION);
        shuffle($output);
        return $output;
    }
}
