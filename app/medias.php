<?php

/* Autoriser les fichiers SVG */
function az_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'az_mime_types');

add_filter( 'intermediate_image_sizes_advanced', 'az_custom_image_sizes' );
// This will remove the default image sizes and the medium_large size.
function az_custom_image_sizes( $sizes ) {
    unset( $sizes['small']); // 150px
    unset( $sizes['medium']); // 300px
    unset( $sizes['large']); // 1024px
    unset( $sizes['medium_large']); // 768px
    return $sizes;
}

add_theme_support( 'post-thumbnails' );
add_image_size( 'lazy', 50, 50 );
add_image_size( 's', 400 );
add_image_size( 'm', 768 );
add_image_size( 'l', 1366 );
add_image_size( 'xl', 1920 );
