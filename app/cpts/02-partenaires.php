<?php
/**
 * Partenaires
 */
function azelar_cpt_partenaire() {
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Partenaires', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Partenaire', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Partenaires'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Tous les partenaires'),
		'view_item'           => __( 'Voir les partenaires'),
		'add_new_item'        => __( 'Ajouter un nouveau partenaire'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer partenaire'),
		'update_item'         => __( 'Modifier partenaire'),
		'search_items'        => __( 'Rechercher un partenaire'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'partenaire' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-megaphone',
        'supports'           => array( 'title', 'author', 'revisions' ),
    );

    register_post_type( 'partenaire', $args );
}

add_action( 'init', 'azelar_cpt_partenaire' );

/**
 * Catégorie partenaire
 */

function azelar_create_partenaire_categorie_taxonomy() {

    $labels = array(
        'name' => _x( 'Catégorie', 'taxonomy general name' ),
        'singular_name' => _x( 'Catégorie', 'taxonomy singular name' ),
        'search_items' =>  __( 'Chercher catégories' ),
        'all_items' => __( 'Toutes les catégories' ),
        'edit_item' => __( 'Editer catégorie' ),
        'update_item' => __( 'Modifier catégorie' ),
        'add_new_item' => __( 'Ajouter nouvelle catégorie' ),
        'new_item_name' => __( 'Nouveau nom de catégorie' ),
        'menu_name' => __( 'Catégories' ),
    );

    register_taxonomy('partenaire_categorie', array('partenaire'), array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'meta_box_cb' => false,
        'show_in_rest' => false,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'pcat' ),
    ));

}

add_action( 'init', 'azelar_create_partenaire_categorie_taxonomy' );

function get_partenaires($cat_id) {
    $posts = get_posts(array(
        'post_type' => 'partenaire',
        'numberposts' => -1,
        'tax_query' => array(array(
            'taxonomy' => 'partenaire_categorie',
            'field'    => 'term_id',
            'terms'    => $cat_id,
        ))
    ));
    $output = array();
    foreach($posts as $post) {
        $tmp =  get_fields($post->ID);
        $tmp['name'] = $post->post_title;
        $output[] = $tmp;
    }
    return $output;
}


add_action( 'template_redirect', 'azelar_redirect_post_partenaire' );

function azelar_redirect_post_partenaire() {
  if ( is_singular( 'partenaire' ) || is_tax('partenaire_categorie')) {
    wp_redirect( home_url(), 301 );
    exit;
  }
}
