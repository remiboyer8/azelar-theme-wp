<?php
/**
 * Entrepreneur
 */
function azelar_cpt_entrepreneur() {
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Entrepreneurs', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Entrepreneur', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Entrepreneurs'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Tous les entrepreneurs'),
		'view_item'           => __( 'Voir la fiche entrepreneur'),
		'add_new_item'        => __( 'Ajouter un nouvel entrepreneur'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer entrepreneur'),
		'update_item'         => __( 'Modifier entrepreneur'),
		'search_items'        => __( 'Rechercher un entrepreneur'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'entrepreneur' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-admin-users',
        'supports'           => array( 'title', 'author', 'revisions' ),
    );

    register_post_type( 'entrepreneur', $args );
}

add_action( 'init', 'azelar_cpt_entrepreneur' );

/**
 * Catégorie pôle métier
 */

function azelar_create_poles_metiers_taxonomy() {

    $labels = array(
        'name' => _x( 'Pôles métiers', 'taxonomy general name' ),
        'singular_name' => _x( 'Pôle métier', 'taxonomy singular name' ),
        'search_items' =>  __( 'Chercher pôles métiers' ),
        'all_items' => __( 'Tous les pôles métiers' ),
        'edit_item' => __( 'Editer pôle métier' ),
        'update_item' => __( 'Modifier pôle métier' ),
        'add_new_item' => __( 'Ajouter nouveau pôle métier' ),
        'new_item_name' => __( 'Nouveau nom de pôle métier' ),
        'menu_name' => __( 'Pôles métiers' ),
    );

    register_taxonomy('poles_metiers', array('entrepreneur'), array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'meta_box_cb' => false,
        'show_in_rest' => false,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'entrepreneurs', 'with_front' => false ),
    ));

}

add_action( 'init', 'azelar_create_poles_metiers_taxonomy' );

/**
 * Etiquettes métiers
 */

function azelar_create_metiers_taxonomy() {

    $labels = array(
        'name' => _x( 'Métiers', 'taxonomy general name' ),
        'singular_name' => _x( 'Métier', 'taxonomy singular name' ),
        'search_items' =>  __( 'Chercher métiers' ),
        'all_items' => __( 'Tous les métiers' ),
        'edit_item' => __( 'Editer métier' ),
        'update_item' => __( 'Modifier métier' ),
        'add_new_item' => __( 'Ajouter nouveau métier' ),
        'new_item_name' => __( 'Nouveau nom de métier' ),
        'menu_name' => __( 'Métiers' ),
    );

    register_taxonomy('metiers', array('entrepreneur'), array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'meta_box_cb' => false,
        'show_in_rest' => false,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'metier' ),
    ));

}

add_action( 'init', 'azelar_create_metiers_taxonomy' );
