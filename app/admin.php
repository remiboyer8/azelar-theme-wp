<?php

namespace App;

$whitelist = array(
  '127.0.0.1',
  '::1'
);

if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
  $admColor = 'ocean';
} else {
  $admColor = 'fresh';
}

// change default color scheme if not customized
$customized_color_scheme = get_user_option('custom_admin_color_scheme', get_current_user_id());
if (empty($customized_color_scheme)) {
  update_user_meta(get_current_user_id(), 'admin_color', $admColor);
}

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/**
 * Page options
 */
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Paramètres',
		'menu_title'	=> 'Paramètres Azelar',
		'menu_slug' 	=> 'az-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'position'      => 2
	));
}

// Barre éditeur simplifié
add_filter( 'acf/fields/wysiwyg/toolbars' , function( $toolbars )
{
	// Add a new toolbar called "Very Simple"
	// - this toolbar has only 1 row of buttons
	$toolbars['Basic_1' ] = array();
	$toolbars['Basic_1' ][1] = array( 'link',  'bold' , 'italic' , 'underline', 'strikethrough', 'bullist', 'numlist', 'alignleft', 'aligncenter', 'alignright', 'formatselect', 'undo', 'redo', 'fullscreen', 'source');
    $toolbars['Basic' ][1] = $toolbars['Basic_1' ][1];
// die(var_dump($toolbars));
	// return $toolbars - IMPORTANT!
	return $toolbars;
});


// Shortcodes on ACF
add_filter('acf/format_value', function( $value, $post_id, $field ) {

    if(is_string($value)) {
        return do_shortcode( $value );
    }else {
        return $value;
    }
}, 10, 3);

add_shortcode('azelar', function() {
    return \App\template('partials.logo-text-azelar');
});
add_shortcode('gds', function() {
    return \App\template('partials.logo-text-gds');
});



add_action('init', function() {
    if( !session_id() )
      {
        session_start();
      }
    });

add_action( 'admin_init', function() {
    add_editor_style( 'editor-style.css' );
} );



add_filter( 'wpseo_sitemap_urlimages', function( $images, $post_id ) {
	$attached_images = get_attached_media( 'image', $post_id);
	if($attached_images){
		foreach($attached_images as $attached_image){
			$image_arr = array();
			// $image_arr['src'] = $attached_image->guid;
			$image_arr['src'] = wp_get_attachment_image_url($attached_image->ID, 'full');
			$images[] = $image_arr;
		}
	}
	array_unique($images);
    return $images;
}, 10, 2 );
